/***************************************************************************
 *   Copyright (C) 2007 by Jean-Baptiste Mardelle (jb@kdenlive.org)        *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA          *
 ***************************************************************************/

#include "projectlist.h"

#include "core.h"
#include "projectitem.h"
#include "projectcommands.h"
#include "jobs/proxyclipjob.h"
#include "jobs/cutclipjob.h"
#include "jobs/meltjob.h"
#include "clipmanager.h"
#include "projectlistview.h"
#include "clipstabilize.h"
#include "dialogs/slideshowclip.h"
//#include "clippropertiesmanager.h"
#include "mltcontroller/bincontroller.h"
#include "kdenlivesettings.h"
#include "renderer.h"
#include "doc/kthumb.h"
#include "definitions.h"
#include "timecodedisplay.h"
#include "titler/titlewidget.h"
#include "doc/docclipbase.h"
#include "doc/kdenlivedoc.h"
#include "dialogs/profilesdialog.h"
#include "monitor/monitormanager.h"

#include "ui_colorclip_ui.h"
#include "ui_templateclip_ui.h"
#include "ui_cutjobdialog_ui.h"
#include "ui_scenecutdialog_ui.h"


#include <klocalizedstring.h>
#include <KMessageBox>
#include <KFileItem>
#include <KRecentDirs>
#include <KColorScheme>
#include <KActionCollection>
#include <KPassivePopup>
#include <KConfigGroup>

#include <QDebug>
#include <QDialog>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QAction>
#include <QPixmap>
#include <QMenu>
#include <QProcess>
#include <QHeaderView>
#include <QVBoxLayout>
#include <QtConcurrent>
#include <QFileDialog>
#include <QDialogButtonBox>
#include <QPushButton>

MyMessageWidget::MyMessageWidget(QWidget *parent) : KMessageWidget(parent) {}
MyMessageWidget::MyMessageWidget(const QString &text, QWidget *parent) : KMessageWidget(text, parent) {}


bool MyMessageWidget::event(QEvent* ev) {
    if (ev->type() == QEvent::Hide || ev->type() == QEvent::Close) emit messageClosing();
    return KMessageWidget::event(ev);
}

SmallInfoLabel::SmallInfoLabel(QWidget *parent) : QPushButton(parent)
{
    setFixedWidth(0);
    setFlat(true);
    
    /*QString style = "QToolButton {background-color: %1;border-style: outset;border-width: 2px;
     border-radius: 5px;border-color: beige;}";*/
    m_timeLine = new QTimeLine(500, this);
    QObject::connect(m_timeLine, SIGNAL(valueChanged(qreal)), this, SLOT(slotTimeLineChanged(qreal)));
    QObject::connect(m_timeLine, SIGNAL(finished()), this, SLOT(slotTimeLineFinished()));
    hide();
}

const QString SmallInfoLabel::getStyleSheet(const QPalette &p)
{
    KColorScheme scheme(p.currentColorGroup(), KColorScheme::Window, KSharedConfig::openConfig(KdenliveSettings::colortheme()));
    QColor bg = scheme.background(KColorScheme::LinkBackground).color();
    QColor fg = scheme.foreground(KColorScheme::LinkText).color();
    QString style = QString("QPushButton {padding:2px;background-color: rgb(%1, %2, %3);border-radius: 4px;border: none;color: rgb(%4, %5, %6)}").arg(bg.red()).arg(bg.green()).arg(bg.blue()).arg(fg.red()).arg(fg.green()).arg(fg.blue());
    
    bg = scheme.background(KColorScheme::ActiveBackground).color();
    fg = scheme.foreground(KColorScheme::ActiveText).color();
    style.append(QString("\nQPushButton:hover {padding:2px;background-color: rgb(%1, %2, %3);border-radius: 4px;border: none;color: rgb(%4, %5, %6)}").arg(bg.red()).arg(bg.green()).arg(bg.blue()).arg(fg.red()).arg(fg.green()).arg(fg.blue()));
    
    return style;
}

void SmallInfoLabel::slotTimeLineChanged(qreal value)
{
    setFixedWidth(qMin(value * 2, qreal(1.0)) * sizeHint().width());
    update();
}

void SmallInfoLabel::slotTimeLineFinished()
{
    if (m_timeLine->direction() == QTimeLine::Forward) {
        // Show
        show();
    } else {
        // Hide
        hide();
        setText(QString());
    }
}

void SmallInfoLabel::slotSetJobCount(int jobCount)
{
    if (jobCount > 0) {
        // prepare animation
        setText(i18np("%1 job", "%1 jobs", jobCount));
        setToolTip(i18np("%1 pending job", "%1 pending jobs", jobCount));
        
        //if (!(KGlobalSettings::graphicEffectsLevel() & KGlobalSettings::SimpleAnimationEffects)) {
        if (style()->styleHint(QStyle::SH_Widget_Animate, 0, this)) {
            setFixedWidth(sizeHint().width());
            show();
            return;
        }
        
        if (isVisible()) {
            setFixedWidth(sizeHint().width());
            update();
            return;
        }
        
        setFixedWidth(0);
        show();
        int wantedWidth = sizeHint().width();
        setGeometry(-wantedWidth, 0, wantedWidth, height());
        m_timeLine->setDirection(QTimeLine::Forward);
        if (m_timeLine->state() == QTimeLine::NotRunning) {
            m_timeLine->start();
        }
    }
    else {
        //if (!(KGlobalSettings::graphicEffectsLevel() & KGlobalSettings::SimpleAnimationEffects)) {
        if (style()->styleHint(QStyle::SH_Widget_Animate, 0, this)) {
            setFixedWidth(0);
            hide();
            return;
        }
        // hide
        m_timeLine->setDirection(QTimeLine::Backward);
        if (m_timeLine->state() == QTimeLine::NotRunning) {
            m_timeLine->start();
        }
    }
    
}

ProjectList::ProjectList(QWidget *parent) :
    QWidget(parent)
  , m_render(NULL)
  , m_fps(-1)
  , m_menu(NULL)
  , m_commandStack(NULL)
  , m_openAction(NULL)
  , m_reloadAction(NULL)
  , m_extractAudioAction(NULL)
  , m_transcodeAction(NULL)
  , m_clipsActionsMenu(NULL)
  , m_doc(NULL)
  , m_refreshed(false)
  , m_allClipsProcessed(false)
  , m_thumbnailQueue()
  , m_proxyAction(NULL)
  , m_abortAllJobs(false)
  , m_closing(false)
  , m_invalidClipDialog(NULL)
{
    QVBoxLayout *layout = new QVBoxLayout;
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);
    // setup toolbar
    QFrame *frame = new QFrame;
    frame->setFrameStyle(QFrame::NoFrame);
    QHBoxLayout *box = new QHBoxLayout;
    box->setContentsMargins(0, 0, 0, 0);
    
    KTreeWidgetSearchLine *searchView = new KTreeWidgetSearchLine;
    box->addWidget(searchView);
    
    // small info button for pending jobs
    m_infoLabel = new SmallInfoLabel(this);
    m_infoLabel->setStyleSheet(SmallInfoLabel::getStyleSheet(palette()));
    connect(this, SIGNAL(jobCount(int)), m_infoLabel, SLOT(slotSetJobCount(int)));
    connect(this, SIGNAL(requestClipSelect(QString)), this, SLOT(selectClip(QString)));
    m_jobsMenu = new QMenu();
    connect(m_jobsMenu, SIGNAL(aboutToShow()), this, SLOT(slotPrepareJobsMenu()));
    QAction *cancelJobs = new QAction(i18n("Cancel All Jobs"), this);
    cancelJobs->setCheckable(false);
    connect(cancelJobs, SIGNAL(triggered()), this, SLOT(slotCancelJobs()));
    connect(this, SIGNAL(checkJobProcess()), this, SLOT(slotCheckJobProcess()));
    m_discardCurrentClipJobs = new QAction(i18n("Cancel Current Clip Jobs"), this);
    m_discardCurrentClipJobs->setCheckable(false);
    connect(m_discardCurrentClipJobs, SIGNAL(triggered()), this, SLOT(slotDiscardClipJobs()));
    m_jobsMenu->addAction(cancelJobs);
    m_jobsMenu->addAction(m_discardCurrentClipJobs);
    m_infoLabel->setMenu(m_jobsMenu);
    box->addWidget(m_infoLabel);

    int size = style()->pixelMetric(QStyle::PM_SmallIconSize);
    QSize iconSize(size, size);

    m_addButton = new QToolButton;
    m_addButton->setPopupMode(QToolButton::MenuButtonPopup);
    m_addButton->setAutoRaise(true);
    m_addButton->setIconSize(iconSize);
    box->addWidget(m_addButton);

    m_editButton = new QToolButton;
    m_editButton->setAutoRaise(true);
    m_editButton->setIconSize(iconSize);
    box->addWidget(m_editButton);

    m_deleteButton = new QToolButton;
    m_deleteButton->setAutoRaise(true);
    m_deleteButton->setIconSize(iconSize);
    box->addWidget(m_deleteButton);
    frame->setLayout(box);
    layout->addWidget(frame);

    m_listView = new ProjectListView(this);
    layout->addWidget(m_listView);
    
    m_infoMessage = new MyMessageWidget;
    layout->addWidget(m_infoMessage);
    m_infoMessage->setCloseButtonVisible(true);
    //m_infoMessage->setWordWrap(true);
    m_infoMessage->hide();
    m_logAction = new QAction(i18n("Show Log"), this);
    m_logAction->setCheckable(false);
    connect(m_logAction, SIGNAL(triggered()), this, SLOT(slotShowJobLog()));

    setLayout(layout);
    searchView->setTreeWidget(m_listView);
    int minHeight = qMax(38, QFontMetrics(font()).lineSpacing() * 2);
    m_listView->setIconSize(QSize(minHeight, minHeight));

    connect(this, SIGNAL(processNextThumbnail()), this, SLOT(slotProcessNextThumbnail()));
    connect(m_listView, SIGNAL(projectModified()), this, SIGNAL(projectModified()));
    connect(m_listView, SIGNAL(itemSelectionChanged()), this, SLOT(slotClipSelected()));
    connect(m_listView, SIGNAL(focusMonitor(bool)), this, SIGNAL(raiseClipMonitor(bool)));
    connect(m_listView, SIGNAL(pauseMonitor()), this, SIGNAL(pauseMonitor()));
    connect(m_listView, SIGNAL(requestMenu(QPoint,QTreeWidgetItem*)), this, SLOT(slotContextMenu(QPoint,QTreeWidgetItem*)));
    connect(m_listView, SIGNAL(addClip()), this, SIGNAL(pauseMonitor()));
    connect(m_listView, SIGNAL(addClip()), this, SLOT(slotAddClip()));
    connect(m_listView, SIGNAL(addClip(QList<QUrl>,QString,QString)), this, SLOT(slotAddClip(QList<QUrl>,QString,QString)));
    connect(this, SIGNAL(addClip(QString,QString,QString)), this, SLOT(slotAddClip(QString,QString,QString)));
    connect(m_listView, SIGNAL(addClipCut(QString,int,int)), this, SLOT(slotAddClipCut(QString,int,int)));
    connect(m_listView, SIGNAL(itemChanged(QTreeWidgetItem*,int)), this, SLOT(slotItemEdited(QTreeWidgetItem*,int)));
    connect(m_listView, SIGNAL(showProperties()), this, SLOT(slotEditClip()));
    
    
    connect(this, SIGNAL(cancelRunningJob(QString,stringMap)), this, SLOT(slotCancelRunningJob(QString,stringMap)));
    connect(this, SIGNAL(processLog(QString,int,int,QString)), this, SLOT(slotProcessLog(QString,int,int,QString)));
    
    connect(this, SIGNAL(updateJobStatus(QString,int,int,QString,QString,QString)), this, SLOT(slotUpdateJobStatus(QString,int,int,QString,QString,QString)));
    
    connect(this, SIGNAL(gotProxy(QString)), this, SLOT(slotGotProxyForId(QString)));
    
    m_listViewDelegate = new ItemDelegate(m_listView);
    m_listView->setItemDelegate(m_listViewDelegate);

    //m_clipPropertiesManager = new ClipPropertiesManager(this);
}

ProjectList::~ProjectList()
{
    m_abortAllJobs = true;
    for (int i = 0; i < m_jobList.count(); ++i) {
        m_jobList.at(i)->setStatus(JobAborted);
    }
    m_closing = true;
    m_thumbnailQueue.clear();
    m_jobThreads.waitForFinished();
    m_jobThreads.clearFutures();
    if (!m_jobList.isEmpty()) qDeleteAll(m_jobList);
    m_jobList.clear();
    if (m_menu) delete m_menu;
    m_listView->blockSignals(true);
    m_listView->clear();
    delete m_listViewDelegate;
    delete m_infoMessage;
}

void ProjectList::selectClip(const QString &id)
{
    ProjectItem *item = getItemById(id);
    if (item) m_listView->setCurrentItem(item);
}

void ProjectList::clearSelection()
{
    m_listView->clearSelection();
}

void ProjectList::updateProjectFormat(Timecode t)
{
    m_timecode = t;
}

void ProjectList::slotEditClip()
{
    QList<QTreeWidgetItem *> list = m_listView->selectedItems();
    if (list.isEmpty()) return;
    if (list.count() > 1 || list.at(0)->type() == ProjectFoldeType) {
        editClipSelection(list);
        return;
    }
    ProjectItem *item;
    if (!m_listView->currentItem() || m_listView->currentItem()->type() == ProjectFoldeType)
        return;
    if (m_listView->currentItem()->type() == ProjectSubclipType)
        item = static_cast <ProjectItem*>(m_listView->currentItem()->parent());
    else
        item = static_cast <ProjectItem*>(m_listView->currentItem());
    if (item && (item->flags() & Qt::ItemIsDragEnabled)) {
        emit clipSelected(item->referencedClip());
        //m_clipPropertiesManager->showClipPropertiesDialog(item->referencedClip());
    }
}

void ProjectList::editClipSelection(QList<QTreeWidgetItem *> list)
{
    // Gather all common properties
    QMap <QString, QString> commonproperties;
    QList <DocClipBase *> clipList;
    commonproperties.insert("force_aspect_num", "-");
    commonproperties.insert("force_aspect_den", "-");
    commonproperties.insert("force_fps", "-");
    commonproperties.insert("force_progressive", "-");
    commonproperties.insert("force_tff", "-");
    commonproperties.insert("threads", "-");
    commonproperties.insert("video_index", "-");
    commonproperties.insert("audio_index", "-");
    commonproperties.insert("force_colorspace", "-");
    commonproperties.insert("full_luma", "-");
    QString transparency = "-";

    bool allowDurationChange = true;
    int commonDuration = -1;
    bool hasImages = false;;
    ProjectItem *item;
    for (int i = 0; i < list.count(); ++i) {
        item = NULL;
        if (list.at(i)->type() == ProjectFoldeType) {
            // Add folder items to the list
            int ct = list.at(i)->childCount();
            for (int j = 0; j < ct; ++j) {
                list.append(list.at(i)->child(j));
            }
            continue;
        }
        else if (list.at(i)->type() == ProjectSubclipType)
            item = static_cast <ProjectItem*>(list.at(i)->parent());
        else
            item = static_cast <ProjectItem*>(list.at(i));
        if (!(item->flags() & Qt::ItemIsDragEnabled))
            continue;
        if (item) {
            // check properties
            DocClipBase *clip = item->referencedClip();
            if (clipList.contains(clip)) continue;
            if (clip->clipType() == Image) {
                hasImages = true;
                if (clip->getProperty("transparency").isEmpty() || clip->getProperty("transparency").toInt() == 0) {
                    if (transparency == "-") {
                        // first non transparent image
                        transparency = '0';
                    }
                    else if (transparency == "1") {
                        // we have transparent and non transparent clips
                        transparency = "-1";
                    }
                }
                else {
                    if (transparency == "-") {
                        // first transparent image
                        transparency = '1';
                    }
                    else if (transparency == "0") {
                        // we have transparent and non transparent clips
                        transparency = "-1";
                    }
                }
            }
            if (clip->clipType() != Color && clip->clipType() != Image && clip->clipType() != Text)
                allowDurationChange = false;
            if (allowDurationChange && commonDuration != 0) {
                if (commonDuration == -1)
                    commonDuration = clip->duration().frames(m_fps);
                else if (commonDuration != clip->duration().frames(m_fps))
                    commonDuration = 0;
            }
            clipList.append(clip);
            QMap <QString, QString> clipprops = clip->properties();
            QMapIterator<QString, QString> p(commonproperties);
            while (p.hasNext()) {
                p.next();
                if (p.value().isEmpty()) continue;
                if (clipprops.contains(p.key())) {
                    if (p.value() == "-")
                        commonproperties.insert(p.key(), clipprops.value(p.key()));
                    else if (p.value() != clipprops.value(p.key()))
                        commonproperties.insert(p.key(), QString());
                } else {
                    commonproperties.insert(p.key(), QString());
                }
            }
        }
    }
    if (allowDurationChange)
        commonproperties.insert("out", QString::number(commonDuration));
    if (hasImages)
        commonproperties.insert("transparency", transparency);
    /*QMapIterator<QString, QString> p(commonproperties);
    while (p.hasNext()) {
        p.next();
        //qDebug() << "Result: " << p.key() << " = " << p.value();
    }*/
    if (clipList.isEmpty()) {
        emit displayMessage(i18n("No available clip selected"), -2, ErrorMessage);
    }
    else {
        //m_clipPropertiesManager->showClipPropertiesDialog(clipList, commonproperties);
    }
}

void ProjectList::slotOpenClip()
{
    ProjectItem *item;
    if (!m_listView->currentItem() || m_listView->currentItem()->type() == ProjectFoldeType)
        return;
    if (m_listView->currentItem()->type() == QTreeWidgetItem::UserType + 1)
        item = static_cast <ProjectItem*>(m_listView->currentItem()->parent());
    else
        item = static_cast <ProjectItem*>(m_listView->currentItem());
    if (item) {
        if (item->clipType() == Image) {
            if (KdenliveSettings::defaultimageapp().isEmpty())
                KMessageBox::sorry(QApplication::activeWindow(), i18n("Please set a default application to open images in the Settings dialog"));
            else
                QProcess::startDetached(KdenliveSettings::defaultimageapp(), QStringList() << item->clipUrl().path());
        }
        if (item->clipType() == Audio) {
            if (KdenliveSettings::defaultaudioapp().isEmpty())
                KMessageBox::sorry(QApplication::activeWindow(), i18n("Please set a default application to open audio files in the Settings dialog"));
            else
                QProcess::startDetached(KdenliveSettings::defaultaudioapp(), QStringList() << item->clipUrl().path());
        }
    }
}

void ProjectList::cleanup()
{
    m_listView->clearSelection();
    QTreeWidgetItemIterator it(m_listView);
    ProjectItem *item;
    while (*it) {
        if ((*it)->type() != ProjectClipType) {
            it++;
            continue;
        }
        item = static_cast <ProjectItem *>(*it);
        if (item->numReferences() == 0)
            item->setSelected(true);
        it++;
    }
    slotRemoveClip();
}

void ProjectList::trashUnusedClips()
{
    QTreeWidgetItemIterator it(m_listView);
    ProjectItem *item;
    QStringList ids;
    QStringList urls;
    while (*it) {
        if ((*it)->type() != ProjectClipType) {
            it++;
            continue;
        }
        item = static_cast <ProjectItem *>(*it);
        if (item->numReferences() == 0) {
            ids << item->clipId();
            QUrl url = item->clipUrl();
            if (url.isValid() && !urls.contains(url.path()))
                urls << url.path();
        }
        it++;
    }

    // Check that we don't use the URL in another clip
    QTreeWidgetItemIterator it2(m_listView);
    while (*it2) {
        if ((*it2)->type() != ProjectClipType) {
            it2++;
            continue;
        }
        item = static_cast <ProjectItem *>(*it2);
        if (item->numReferences() > 0) {
            QUrl url = item->clipUrl();
            if (url.isValid() && urls.contains(url.path())) urls.removeAll(url.path());
        }
        it2++;
    }

    emit deleteProjectClips(ids, QMap <QString, QString>());
    for (int i = 0; i < urls.count(); ++i)
        QFile::remove(urls.at(i));
}

void ProjectList::slotReloadClip(const QString &id)
{
    QList<QTreeWidgetItem *> selected;
    if (id.isEmpty())
        selected = m_listView->selectedItems();
    else {
        ProjectItem *itemToReLoad = getItemById(id);
        if (itemToReLoad) selected.append(itemToReLoad);
    }
    ProjectItem *item;
    for (int i = 0; i < selected.count(); ++i) {
        if (selected.at(i)->type() != ProjectClipType) {
            if (selected.at(i)->type() == ProjectFoldeType) {
                for (int j = 0; j < selected.at(i)->childCount(); ++j)
                    selected.append(selected.at(i)->child(j));
            }
            continue;
        }
        item = static_cast <ProjectItem *>(selected.at(i));
        if (item && !hasPendingJob(item, AbstractClipJob::PROXYJOB)) {
            DocClipBase *clip = item->referencedClip();
            if (!clip || !clip->isClean() || m_render->isProcessing(item->clipId())) {
                //qDebug()<<"//// TRYING TO RELOAD: "<<item->clipId()<<", but it is busy";
                continue;
            }
            ClipType t = item->clipType();
            if (t == Text) {
                if (clip && !clip->getProperty("xmltemplate").isEmpty())
                    regenerateTemplate(item);
            } else if (t != Color && t != SlideShow && clip && clip->checkHash() == false) {
                item->referencedClip()->setPlaceHolder(true);
                item->setProperty("file_hash", QString());
            } else if (t == Image) {
                //clip->getProducer() clip->getProducer()->set("force_reload", 1);
            }

            QDomElement e = item->toXml();
            // Make sure we get the correct producer length if it was adjusted in timeline
            if (t == Color || t == Image || t == SlideShow || t == Text) {
                int length = QString(clip->producerProperty("length")).toInt();
                if (length > 0 && !e.hasAttribute("length")) {
                    e.setAttribute("length", length);
                }
                e.setAttribute("duration", clip->getProperty("duration"));
            }
            resetThumbsProducer(clip);
            m_render->getFileProperties(e, item->clipId(), m_listView->iconSize().height(), true);
        }
    }
}

void ProjectList::slotModifiedClip(const QString &id)
{
    ProjectItem *item = getItemById(id);
    if (item) {
        QPixmap pixmap = item->data(0, Qt::DecorationRole).value<QPixmap>();
        if (!pixmap.isNull()) {
            QPainter p(&pixmap);
            p.fillRect(0, 0, pixmap.width(), pixmap.height(), QColor(255, 255, 255, 200));
            p.drawPixmap(0, 0, QIcon::fromTheme("view-refresh").pixmap(m_listView->iconSize()));
            p.end();
        } else {
            pixmap = QIcon::fromTheme("view-refresh").pixmap(m_listView->iconSize());
        }
        item->setPixmap(pixmap);
    }
}

void ProjectList::slotMissingClip(const QString &id)
{
    ProjectItem *item = getItemById(id);
    if (item) {
        item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsDropEnabled);
        int height = m_listView->iconSize().height();
        if (m_render == NULL) {
            //qDebug() << "*********  ERROR, NULL RENDR";
            return;
        }
        int width = (int)(height  * m_render->dar());
        QPixmap pixmap = item->data(0, Qt::DecorationRole).value<QPixmap>();
        if (pixmap.isNull()) {
            pixmap = QPixmap(width, height);
            pixmap.fill(Qt::transparent);
        }
        QIcon icon = QIcon::fromTheme("dialog-close");
        QPainter p(&pixmap);
        p.drawPixmap(3, 3, icon.pixmap(width - 6, height - 6));
        p.end();
        item->setPixmap(pixmap);
        if (item->referencedClip()) {
            item->referencedClip()->setPlaceHolder(true);
            Mlt::Producer *newProd = m_render->invalidProducer(id);
            if (item->referencedClip()->getProducer()) {
                Mlt::Properties props(newProd->get_properties());
                Mlt::Properties src_props(item->referencedClip()->getProducer()->get_properties());
                props.inherit(src_props);
            }
            item->referencedClip()->setProducer(*newProd, true);
            item->slotSetToolTip();
            emit clipNeedsReload(id);
        }
    }
    update();
    emit displayMessage(i18n("Check missing clips"), -2, ErrorMessage);
    emit updateRenderStatus();
}

void ProjectList::slotAvailableClip(const QString &id)
{
    ProjectItem *item = getItemById(id);
    if (item == NULL)
        return;
    item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsDragEnabled | Qt::ItemIsEnabled | Qt::ItemIsEditable | Qt::ItemIsDropEnabled);
    if (item->referencedClip()) { // && item->referencedClip()->checkHash() == false) {
        item->setProperty("file_hash", QString());
        slotReloadClip(id);
    }
    /*else {
    item->referencedClip()->setValid();
    item->slotSetToolTip();
    }
    update();*/
    emit updateRenderStatus();
}

bool ProjectList::hasMissingClips()
{
    bool missing = false;
    QTreeWidgetItemIterator it(m_listView);
    while (*it) {
        if ((*it)->type() == ProjectClipType && !((*it)->flags() & Qt::ItemIsDragEnabled)) {
            missing = true;
            break;
        }
        it++;
    }
    return missing;
}

void ProjectList::slotClipSelected()
{
    QTreeWidgetItem *item = m_listView->currentItem();
    ProjectItem *clip = NULL;
    if (item) {
        if (item->type() == ProjectFoldeType) {
	    // User clicked on a folder item
            emit clipSelected(NULL);
            m_editButton->defaultAction()->setEnabled(item->childCount() > 0);
            m_deleteButton->defaultAction()->setEnabled(true);
            m_openAction->setEnabled(false);
            m_reloadAction->setEnabled(false);
            m_extractAudioAction->setEnabled(false);
            m_transcodeAction->setEnabled(false);
            m_clipsActionsMenu->setEnabled(false);
        } else if (item->type() == ProjectSubclipType) {
            // this is a sub item, use base clip
            m_deleteButton->defaultAction()->setEnabled(true);
            clip = static_cast <ProjectItem*>(item->parent());
            if (clip == NULL) {
                //qDebug() << "-----------ERROR";
                return;
            }
            SubProjectItem *sub = static_cast <SubProjectItem*>(item);
            //if (clip->referencedClip()->getProducer() == NULL) m_render->getFileProperties(clip->referencedClip()->toXML(), clip->clipId(), m_listView->iconSize().height(), true);
	    if (!pCore->binController()->hasClip(clip->clipId())) m_render->getFileProperties(clip->referencedClip()->toXML(), clip->clipId(), m_listView->iconSize().height(), true);
            emit clipSelected(clip->referencedClip(), sub->zone());
            m_extractAudioAction->setEnabled(false);
            m_transcodeAction->setEnabled(false);
            m_clipsActionsMenu->setEnabled(false);
            m_reloadAction->setEnabled(false);
            adjustProxyActions(clip);
            return;
        } else {
	    // User selected a clip
            clip = static_cast <ProjectItem*>(item);
            if (clip == NULL) {
                //qDebug() << "-----------ERROR";
                return;
            }
            if (clip->referencedClip())
                emit clipSelected(clip->referencedClip());
            //if (clip->referencedClip()->getProducer() == NULL) m_render->getFileProperties(clip->referencedClip()->toXML(), clip->clipId(), m_listView->iconSize().height(), true);
	    //if (clip->referencedClip()->binIndex() == -1) m_render->getFileProperties(clip->referencedClip()->toXML(), clip->clipId(), m_listView->iconSize().height(), true);
            m_editButton->defaultAction()->setEnabled(true);
            m_deleteButton->defaultAction()->setEnabled(true);
            m_reloadAction->setEnabled(true);
            m_extractAudioAction->setEnabled(true);
            m_transcodeAction->setEnabled(true);
            m_clipsActionsMenu->setEnabled(true);
            if (clip->clipType() == Image && !KdenliveSettings::defaultimageapp().isEmpty()) {
                m_openAction->setIcon(QIcon::fromTheme(KdenliveSettings::defaultimageapp()));
                m_openAction->setEnabled(true);
            } else if (clip->clipType() == Audio && !KdenliveSettings::defaultaudioapp().isEmpty()) {
                m_openAction->setIcon(QIcon::fromTheme(KdenliveSettings::defaultaudioapp()));
                m_openAction->setEnabled(true);
            } else {
                m_openAction->setEnabled(false);
            }
            // Display relevant transcoding actions only
            adjustTranscodeActions(clip);
            adjustStabilizeActions(clip);
            // Display uses in timeline
            emit findInTimeline(clip->clipId());
        }
    } else {
        emit clipSelected(NULL);
        m_editButton->defaultAction()->setEnabled(false);
        m_deleteButton->defaultAction()->setEnabled(false);
        m_openAction->setEnabled(false);
        m_reloadAction->setEnabled(false);
        m_extractAudioAction->setEnabled(true);
        m_transcodeAction->setEnabled(false);
        m_clipsActionsMenu->setEnabled(false);
    }
    adjustProxyActions(clip);
}

void ProjectList::adjustProxyActions(ProjectItem *clip) const
{
    if (!m_proxyAction) return;
    if (clip == NULL || clip->type() != ProjectClipType || clip->clipType() == Color || clip->clipType() == Text || clip->clipType() == SlideShow || clip->clipType() == Audio) {
        m_proxyAction->setEnabled(false);
        return;
    }
    bool enabled = useProxy();
    if (clip->referencedClip() && !clip->referencedClip()->getProperty("_missingsource").isEmpty()) enabled = false;
    m_proxyAction->setEnabled(enabled);
    m_proxyAction->blockSignals(true);
    m_proxyAction->setChecked(clip->hasProxy());
    m_proxyAction->blockSignals(false);
}

void ProjectList::adjustStabilizeActions(ProjectItem *clip) const
{

    if (clip == NULL || clip->type() != ProjectClipType || clip->clipType() == Color || clip->clipType() == Text || clip->clipType() == SlideShow) {
        m_clipsActionsMenu->setEnabled(false);
        return;
    }
    m_clipsActionsMenu->setEnabled(true);

}

void ProjectList::adjustTranscodeActions(ProjectItem *clip) const
{
    if (clip == NULL || clip->type() != ProjectClipType || clip->clipType() == Color || clip->clipType() == Text || clip->clipType() == Playlist || clip->clipType() == SlideShow) {
        m_transcodeAction->setEnabled(false);
        m_extractAudioAction->setEnabled(false);
        return;
    }
    m_transcodeAction->setEnabled(true);
    m_extractAudioAction->setEnabled(true);
    QList<QAction *> transcodeActions = m_transcodeAction->actions();
    QStringList data;
    QString condition;
    for (int i = 0; i < transcodeActions.count(); ++i) {
        data = transcodeActions.at(i)->data().toStringList();
        if (data.count() > 2) {
            condition = data.at(2);
            if (condition.startsWith(QLatin1String("vcodec")))
                transcodeActions.at(i)->setEnabled(clip->referencedClip()->hasVideoCodec(condition.section('=', 1, 1)));
            else if (condition.startsWith(QLatin1String("acodec")))
                transcodeActions.at(i)->setEnabled(clip->referencedClip()->hasVideoCodec(condition.section('=', 1, 1)));
        }
    }

}

void ProjectList::slotPauseMonitor()
{
    if (m_render)
        m_render->pause();
}

void ProjectList::slotUpdateClipProperties(const QString &id, QMap <QString, QString> properties)
{
    ProjectItem *item = getItemById(id);
    if (item) {
        slotUpdateClipProperties(item, properties);
        if (properties.contains("out") || properties.contains("force_fps") || properties.contains("resource") || properties.contains("video_index") || properties.contains("audio_index") || properties.contains("full_luma") || properties.contains("force_progressive") || properties.contains("force_tff") || properties.contains("force_colorspace")) {
            slotReloadClip(id);
        } else if (properties.contains("colour") ||
                   properties.contains("xmldata") ||
                   properties.contains("force_aspect_num") ||
                   properties.contains("force_aspect_den") ||
                   properties.contains("templatetext")) {
            slotRefreshClipThumbnail(item);
            emit refreshClip(id, true);
        } else if (properties.contains("loop")) {
            emit refreshClip(id, false);
        }
    }
}

void ProjectList::slotUpdateClipProperties(ProjectItem *clip, QMap <QString, QString> properties)
{
    if (!clip)
        return;
    clip->setProperties(properties);
    if (properties.contains("kdenlive:proxy")) {
        if (properties.value("kdenlive:proxy") == "-" || properties.value("kdenlive:proxy").isEmpty())
            // this should only apply to proxy jobs
            clip->setConditionalJobStatus(NoJob, AbstractClipJob::PROXYJOB);
    }
    if (properties.contains("name")) {
        monitorItemEditing(false);
        clip->setText(0, properties.value("name"));
        monitorItemEditing(true);
        emit clipNameChanged(clip->clipId(), properties.value("name"));
    }
    if (properties.contains("description")) {
        emit projectModified();
    }
}

void ProjectList::slotItemEdited(QTreeWidgetItem *item, int column)
{
    if (item->type() == ProjectSubclipType) {
        // this is a sub-item
        if (column == 1) {
            // user edited description
            SubProjectItem *sub = static_cast <SubProjectItem*>(item);
            ProjectItem *item = static_cast <ProjectItem *>(sub->parent());
            EditClipCutCommand *command = new EditClipCutCommand(this, item->clipId(), sub->zone(), sub->zone(), sub->description(), sub->text(1), true);
            m_commandStack->push(command);
            //slotUpdateCutClipProperties(sub->clipId(), sub->zone(), sub->text(1), sub->text(1));
        }
        return;
    }
    if (item->type() == ProjectFoldeType) {
        if (column == 0) {
            FolderProjectItem *folder = static_cast <FolderProjectItem*>(item);
            if (item->text(0) == folder->groupName()) return;
            editFolder(item->text(0), folder->groupName(), folder->clipId());
            folder->setGroupName(item->text(0));
            //m_doc->clipManager()->addFolder(folder->clipId(), item->text(0));
            const int children = item->childCount();
            for (int i = 0; i < children; ++i) {
                ProjectItem *child = static_cast <ProjectItem *>(item->child(i));
                child->setProperty("groupname", item->text(0));
            }
        }
        return;
    }

    ProjectItem *clip = static_cast <ProjectItem*>(item);
    if (column == 1) {
        if (clip->referencedClip()) {
            QMap <QString, QString> oldprops;
            QMap <QString, QString> newprops;
            oldprops["description"] = clip->referencedClip()->getProperty("description");
            newprops["description"] = item->text(1);

            if (clip->clipType() == Text) {
                // This is a text template clip, update the image
                oldprops.insert("templatetext", clip->referencedClip()->getProperty("templatetext"));
                newprops.insert("templatetext", item->text(1));
            }
            slotUpdateClipProperties(clip->clipId(), newprops);
	    //TODO
            /*EditClipCommand *command = new EditClipCommand(this, clip->clipId(), oldprops, newprops, false);
            m_commandStack->push(command);*/
        }
    } else if (column == 0) {
        if (clip->referencedClip()) {
            QMap <QString, QString> oldprops;
            QMap <QString, QString> newprops;
            oldprops["name"] = clip->referencedClip()->getProperty("name");
            if (oldprops.value("name") != item->text(0)) {
                newprops["name"] = item->text(0);
                slotUpdateClipProperties(clip, newprops);
                emit projectModified();
		//TODO
                /*EditClipCommand *command = new EditClipCommand(this, clip->clipId(), oldprops, newprops, false);
                m_commandStack->push(command);*/
                QTimer::singleShot(100, this, SLOT(slotCheckScrolling()));
            }
        }
    }
}

void ProjectList::slotCheckScrolling()
{
    m_listView->scrollToItem(m_listView->currentItem());
}

void ProjectList::slotContextMenu(const QPoint &pos, QTreeWidgetItem *item)
{
    if (!m_menu) {
        //qDebug()<<"Warning, menu was not created, something is wrong";
        return;
    }
    bool enable = item ? true : false;
    m_editButton->defaultAction()->setEnabled(enable);
    m_deleteButton->defaultAction()->setEnabled(enable);
    m_reloadAction->setEnabled(enable);
    m_extractAudioAction->setEnabled(enable);
    m_transcodeAction->setEnabled(enable);
    m_clipsActionsMenu->setEnabled(enable);
    if (enable) {
        ProjectItem *clip = NULL;
        if (m_listView->currentItem()->type() == ProjectSubclipType) {
            clip = static_cast <ProjectItem*>(item->parent());
            m_extractAudioAction->setEnabled(false);
            m_transcodeAction->setEnabled(false);
            m_clipsActionsMenu->setEnabled(false);
            adjustProxyActions(clip);
        } else if (m_listView->currentItem()->type() == ProjectClipType) {
            clip = static_cast <ProjectItem*>(item);
            // Display relevant transcoding actions only
            adjustTranscodeActions(clip);
            adjustStabilizeActions(clip);
            adjustProxyActions(clip);
            // Display uses in timeline
            emit findInTimeline(clip->clipId());
        } else {
            m_extractAudioAction->setEnabled(false);
            m_transcodeAction->setEnabled(false);
            m_clipsActionsMenu->setEnabled(false);
        }
        if (clip && clip->clipType() == Image && !KdenliveSettings::defaultimageapp().isEmpty()) {
            m_openAction->setIcon(QIcon::fromTheme(KdenliveSettings::defaultimageapp()));
            m_openAction->setEnabled(true);
        } else if (clip && clip->clipType() == Audio && !KdenliveSettings::defaultaudioapp().isEmpty()) {
            m_openAction->setIcon(QIcon::fromTheme(KdenliveSettings::defaultaudioapp()));
            m_openAction->setEnabled(true);
        } else {
            m_openAction->setEnabled(false);
        }

    } else {
        m_openAction->setEnabled(false);
    }
    m_menu->popup(pos);
}

void ProjectList::slotRemoveClip()
{
    if (!m_listView->currentItem())
        return;
    QStringList ids;
    QMap <QString, QString> folderids;
    QList<QTreeWidgetItem *> selected = m_listView->selectedItems();

    QUndoCommand *delCommand = new QUndoCommand();
    delCommand->setText(i18n("Delete Clip Zone"));
    for (int i = 0; i < selected.count(); ++i) {
        if (selected.at(i)->type() == ProjectSubclipType) {
            // subitem
            SubProjectItem *sub = static_cast <SubProjectItem *>(selected.at(i));
            ProjectItem *item = static_cast <ProjectItem *>(sub->parent());
            new AddClipCutCommand(this, item->clipId(), sub->zone().x(), sub->zone().y(), sub->description(), false, true, delCommand);
        } else if (selected.at(i)->type() == ProjectFoldeType) {
            // folder
            FolderProjectItem *folder = static_cast <FolderProjectItem *>(selected.at(i));
            folderids[folder->groupName()] = folder->clipId();
            int children = folder->childCount();

            if (children > 0 && KMessageBox::questionYesNo(QApplication::activeWindow(), i18np("Delete folder <b>%2</b>?<br />This will also remove the clip in that folder", "Delete folder <b>%2</b>?<br />This will also remove the %1 clips in that folder",  children, folder->text(1)), i18n("Delete Folder")) != KMessageBox::Yes)
                return;
            for (int i = 0; i < children; ++i) {
                ProjectItem *child = static_cast <ProjectItem *>(folder->child(i));
                ids << child->clipId();
            }
        } else {
            ProjectItem *item = static_cast <ProjectItem *>(selected.at(i));
            ids << item->clipId();
            if (item->numReferences() > 0 && KMessageBox::questionYesNo(QApplication::activeWindow(), i18np("Delete clip <b>%2</b>?<br />This will also remove the clip in timeline", "Delete clip <b>%2</b>?<br />This will also remove its %1 clips in timeline", item->numReferences(), item->text(1)), i18n("Delete Clip"), KStandardGuiItem::yes(), KStandardGuiItem::no(), "DeleteAll") == KMessageBox::No) {
                KMessageBox::enableMessage("DeleteAll");
                return;
            }
        }
    }
    KMessageBox::enableMessage("DeleteAll");
    if (delCommand->childCount() == 0)
        delete delCommand;
    else
        m_commandStack->push(delCommand);
    emit deleteProjectClips(ids, folderids);
}

void ProjectList::updateButtons() const
{
    return;
    /* TODO
    if (m_listView->topLevelItemCount() == 0) {
        m_deleteButton->defaultAction()->setEnabled(false);
        m_editButton->defaultAction()->setEnabled(false);
    } else {
        m_deleteButton->defaultAction()->setEnabled(true);
        if (!m_listView->currentItem())
            m_listView->setCurrentItem(m_listView->topLevelItem(0));
        QTreeWidgetItem *item = m_listView->currentItem();
        if (item && item->type() == ProjectClipType) {
            m_editButton->defaultAction()->setEnabled(true);
            m_openAction->setEnabled(true);
            if (m_reloadAction) m_reloadAction->setEnabled(false);
            m_transcodeAction->setEnabled(true);
            m_clipsActionsMenu->setEnabled(true);
            return;
        }
        else if (item && item->type() == ProjectFoldeType && item->childCount() > 0) {
            m_editButton->defaultAction()->setEnabled(true);
        }
        else m_editButton->defaultAction()->setEnabled(false);
    }
    m_openAction->setEnabled(false);
    if (m_reloadAction) m_reloadAction->setEnabled(false);
    m_transcodeAction->setEnabled(false);
    m_clipsActionsMenu->setEnabled(false);
    if (m_proxyAction) m_proxyAction->setEnabled(false);
    */
}

void ProjectList::slotDeleteClip(const QString &clipId)
{
    ProjectItem *item = getItemById(clipId);
    if (!item) {
        //qDebug() << "/// Cannot find clip to delete";
        return;
    }
    deleteJobsForClip(clipId);
    m_listView->blockSignals(true);
    QTreeWidgetItem *newSelectedItem = m_listView->itemAbove(item);
    if (!newSelectedItem)
        newSelectedItem = m_listView->itemBelow(item);
    // Pause playing to prevent crash while deleting clip
    slotPauseMonitor();
    m_doc->clipManager()->deleteClip(clipId);
    delete item;
    m_listView->blockSignals(false);
    if (newSelectedItem) {
        m_listView->setCurrentItem(newSelectedItem);
    } else {
        updateButtons();
        emit clipSelected(NULL);
    }
}


void ProjectList::editFolder(const QString folderName, const QString oldfolderName, const QString &clipId)
{
    EditFolderCommand *command = new EditFolderCommand(this, folderName, oldfolderName, clipId, false);
    m_commandStack->push(command);
}

void ProjectList::slotAddFolder(const QString &name)
{
    AddFolderCommand *command = new AddFolderCommand(this, name.isEmpty() ? i18n("Folder") : name, QString::number(m_doc->clipManager()->getFreeFolderId()), true);
    m_commandStack->push(command);
}

void ProjectList::slotAddFolder(const QString &foldername, const QString &clipId, bool remove, bool edit)
{
    if (remove) {
        FolderProjectItem *item = getFolderItemById(clipId);
        if (item) {
            //m_doc->clipManager()->deleteFolder(clipId);
            QTreeWidgetItem *newSelectedItem = m_listView->itemAbove(item);
            if (!newSelectedItem)
                newSelectedItem = m_listView->itemBelow(item);
            delete item;
            if (newSelectedItem)
                m_listView->setCurrentItem(newSelectedItem);
            else
                updateButtons();
        }
    } else {
        if (edit) {
            FolderProjectItem *item = getFolderItemById(clipId);
            if (item) {
                m_listView->blockSignals(true);
                item->setGroupName(foldername);
                m_listView->blockSignals(false);
                //m_doc->clipManager()->addFolder(clipId, foldername);
                const int children = item->childCount();
                for (int i = 0; i < children; ++i) {
                    ProjectItem *child = static_cast <ProjectItem *>(item->child(i));
                    child->setProperty("groupname", foldername);
                }
            }
        } else {
            m_listView->blockSignals(true);
            m_listView->setCurrentItem(new FolderProjectItem(m_listView, QStringList() << foldername, clipId));
            //m_doc->clipManager()->addFolder(clipId, foldername);
            m_listView->blockSignals(false);
            m_listView->editItem(m_listView->currentItem(), 0);
        }
        updateButtons();
    }
}

void ProjectList::slotGotProxy(const QString &proxyPath)
{
    if (proxyPath.isEmpty() || m_abortAllJobs) return;
    QTreeWidgetItemIterator it(m_listView);
    ProjectItem *item;

    while (*it && !m_closing) {
        if ((*it)->type() == ProjectClipType) {
            item = static_cast <ProjectItem *>(*it);
            if (item->referencedClip()->getProperty("proxy") == proxyPath)
                slotGotProxy(item);
        }
        ++it;
    }
}

void ProjectList::slotGotProxyForId(const QString id)
{
    if (m_closing) return;
    ProjectItem *item = getItemById(id);
    slotGotProxy(item);
}

void ProjectList::slotGotProxy(ProjectItem *item)
{
    if (item == NULL) return;
    DocClipBase *clip = item->referencedClip();
    if (!clip || !clip->isClean() || m_render->isProcessing(item->clipId())) {
        // Clip is being reprocessed, abort
        //qDebug()<<"//// TRYING TO PROXY: "<<item->clipId()<<", but it is busy";
        return;
    }
    
    // Proxy clip successfully created
    QDomElement e = clip->toXML().cloneNode().toElement();

    // Make sure we get the correct producer length if it was adjusted in timeline
    ClipType t = item->clipType();
    if (t == Color || t == Image || t == SlideShow || t == Text) {
        int length = QString(clip->producerProperty("length")).toInt();
        if (length > 0 && !e.hasAttribute("length")) {
            e.setAttribute("length", length);
        }
    }
    resetThumbsProducer(clip);
    m_render->getFileProperties(e, clip->getId(), m_listView->iconSize().height(), true);
}

void ProjectList::slotUpdateClip(const QString &id)
{
    ProjectItem *item = getItemById(id);
    monitorItemEditing(false);
    if (item){
        item->setData(0, ItemDelegate::UsageRole, QString::number(item->numReferences()));
    }
    monitorItemEditing(true);
}

void ProjectList::getCachedThumbnail(ProjectItem *item)
{
    if (!item) return;
    DocClipBase *clip = item->referencedClip();
    if (!clip) {
        return;
    }
    QString cachedPixmap = m_doc->projectFolder().path() + QDir::separator() + "thumbs/" + clip->getClipHash() + ".png";
    if (QFile::exists(cachedPixmap)) {
        QPixmap pix(cachedPixmap);
        if (pix.isNull()) {
            QFile::remove(cachedPixmap);
            requestClipThumbnail(item->clipId());
        }
        else {
            QPixmap result = roundedPixmap(pix);
            processThumbOverlays(item, result);
            item->setPixmap(result);
        }
    }
    else {
        requestClipThumbnail(item->clipId());
    }
}

QPixmap ProjectList::roundedPixmap(const QImage &img)
{
    QPixmap pix(img.width(), img.height());
    pix.fill(Qt::transparent);
    QPainter p(&pix);
    p.setRenderHint(QPainter::Antialiasing, true);
    QPainterPath path;
    path.addRoundedRect(0.5, 0.5, pix.width() - 1, pix.height() - 1, 2, 2);
    p.setClipPath(path);
    p.drawImage(0, 0, img);
    p.end();
    return pix;
}

QPixmap ProjectList::roundedPixmap(const QPixmap &source)
{
    QPixmap pix(source.width(), source.height());
    pix.fill(Qt::transparent);
    QPainter p(&pix);
    p.setRenderHint(QPainter::Antialiasing, true);
    QPainterPath path;
    path.addRoundedRect(0.5, 0.5, pix.width() - 1, pix.height() - 1, 2, 2);
    p.setClipPath(path);
    p.drawPixmap(0, 0, source);
    p.end();
    return pix;
}

void ProjectList::getCachedThumbnail(SubProjectItem *item)
{
    if (!item) return;
    ProjectItem *parentItem = static_cast <ProjectItem *>(item->parent());
    if (!parentItem) return;
    DocClipBase *clip = parentItem->referencedClip();
    if (!clip) return;
    int pos = item->zone().x();
    QString cachedPixmap = m_doc->projectFolder().path() + QDir::separator() + "thumbs/" + clip->getClipHash() + '#' + QString::number(pos) + ".png";
    if (QFile::exists(cachedPixmap)) {
        QPixmap pix(cachedPixmap);
        if (pix.isNull()) {
            QFile::remove(cachedPixmap);
            requestClipThumbnail(parentItem->clipId() + '#' + QString::number(pos));
        }
        else item->setData(0, Qt::DecorationRole, pix);
    }
    else requestClipThumbnail(parentItem->clipId() + '#' + QString::number(pos));
}

void ProjectList::updateAllClips(bool displayRatioChanged, bool fpsChanged, const QStringList &brokenClips)
{
    /*
    if (!m_allClipsProcessed) m_listView->setEnabled(false);
    m_listView->setSortingEnabled(false);
    QTreeWidgetItemIterator it(m_listView);
    DocClipBase *clip;
    ProjectItem *item;
    monitorItemEditing(false);
    int height = m_listView->iconSize().height();
    int width = (int)(height  * m_render->dar());
    QPixmap missingPixmap = QPixmap(width, height);
    missingPixmap.fill(Qt::transparent);
    QIcon icon("dialog-close");
    QPainter p(&missingPixmap);
    p.drawPixmap(3, 3, icon.pixmap(width - 6, height - 6));
    p.end();
    
    int max = m_doc->clipManager()->clipsCount();
    max = qMax(1, max);
    int ct = 0;

    while (*it) {
        emit displayMessage(i18n("Loading thumbnails"), (int)(100 *(max - ct++) / max));
        if ((*it)->type() == ProjectSubclipType) {
            // subitem
            SubProjectItem *sub = static_cast <SubProjectItem *>(*it);
            if (displayRatioChanged) {
                item = static_cast <ProjectItem *>((*it)->parent());
                requestClipThumbnail(item->clipId() + '#' + QString::number(sub->zone().x()));
            }
            else if (sub->data(0, Qt::DecorationRole).isNull()) {
                getCachedThumbnail(sub);
            }
            ++it;
            continue;
        } else if ((*it)->type() == ProjectFoldeType) {
            // folder
            ++it;
            continue;
        } else {
            item = static_cast <ProjectItem *>(*it);
            clip = item->referencedClip();
            //if (clip->getProducer() == NULL) {
	    if (!pCore->binController()->hasClip(clip->getId())) {
                bool replace = false;
                if (brokenClips.contains(item->clipId())) {
                    // if this is a proxy clip, disable proxy
                    item->setConditionalJobStatus(NoJob, AbstractClipJob::PROXYJOB);
                    discardJobs(item->clipId(), AbstractClipJob::PROXYJOB);
                    clip->setProperty("proxy", "-");
                    replace = true;
                }
                if (clip->isPlaceHolder() == false && !hasPendingJob(item, AbstractClipJob::PROXYJOB)) {
                    QDomElement xml = clip->toXML();
                    getCachedThumbnail(item);
                    if (fpsChanged) {
                        xml.removeAttribute("out");
                        xml.removeAttribute("file_hash");
                        xml.removeAttribute("proxy_out");
                    }
                    if (!replace) replace = xml.attribute("_replaceproxy") == "1";
                    xml.removeAttribute("_replaceproxy");
                    if (replace) {
                        resetThumbsProducer(clip);
                        m_render->getFileProperties(xml, clip->getId(), m_listView->iconSize().height(), replace);
                    }
                    else if (item->numReferences() > 0) {
                        // In some cases, like slowmotion clips, the producer is not loaded automatically be MLT
                        m_render->getFileProperties(xml, clip->getId(), m_listView->iconSize().height(), replace);
                    }
                }
                else if (clip->isPlaceHolder()) {
                    item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsDropEnabled);
                    if (!item->hasPixmap()) {
                        item->setPixmap(missingPixmap);
                    }
                    else {
                        QPixmap pixmap = item->data(0, Qt::DecorationRole).value<QPixmap>();
                        QPainter p(&pixmap);
                        p.drawPixmap(3, 3, QIcon::fromTheme("dialog-close").pixmap(pixmap.width() - 6, pixmap.height() - 6));
                        p.end();
                        item->setPixmap(pixmap);
                    }
                }
            } else {
                if (displayRatioChanged) {
                    requestClipThumbnail(clip->getId());
                }
                else if (!item->hasPixmap()) {
                    getCachedThumbnail(item);
                }
                if (item->data(0, ItemDelegate::DurationRole).toString().isEmpty()) {
                    //item->changeDuration(clip->getProducer()->get_playtime());
		    //item->changeDuration(pCore->binController()->getBinClipDuration(clip->getId()));
                }
                if (clip->isPlaceHolder()) {
                    QPixmap pixmap = item->data(0, Qt::DecorationRole).value<QPixmap>();
                    if (pixmap.isNull()) {
                        pixmap = QPixmap(width, height);
                        pixmap.fill(Qt::transparent);
                    }
                    QPainter p(&pixmap);
                    p.drawPixmap(3, 3, QIcon::fromTheme("dialog-close").pixmap(pixmap.width() - 6, pixmap.height() - 6));
                    p.end();
                    item->setPixmap(pixmap);
                }
                else if (clip->getProperty("_replaceproxy") == "1") {
                    clip->setProperty("_replaceproxy", QString());
                    slotCreateProxy(clip->getId());
                }
            }
            item->setData(0, ItemDelegate::UsageRole, QString::number(item->numReferences()));
        }
        ++it;
    }

    m_listView->setSortingEnabled(true);
    m_allClipsProcessed = true;
    if (m_render->processingItems() == 0) {
        monitorItemEditing(true);
        slotProcessNextThumbnail();
    }
    */
}

// static
QStringList ProjectList::getExtensions()
{
    // Build list of mime types
    QStringList mimeTypes = QStringList() << "application/x-kdenlive" << "application/x-kdenlivetitle" << "video/mlt-playlist" << "text/plain";

    // Video mimes
    mimeTypes <<  "video/x-flv" << "application/vnd.rn-realmedia" << "video/x-dv" << "video/dv" << "video/x-msvideo" << "video/x-matroska" << "video/mpeg" << "video/ogg" << "video/x-ms-wmv" << "video/mp4" << "video/quicktime" << "video/webm" << "video/3gpp" << "video/mp2t";

    // Audio mimes
    mimeTypes << "audio/x-flac" << "audio/x-matroska" << "audio/mp4" << "audio/mpeg" << "audio/x-mp3" << "audio/ogg" << "audio/x-wav" << "audio/x-aiff" << "audio/aiff" << "application/ogg" << "application/mxf" << "application/x-shockwave-flash" << "audio/ac3";

    // Image mimes
    mimeTypes << "image/gif" << "image/jpeg" << "image/png" << "image/x-tga" << "image/x-bmp" << "image/svg+xml" << "image/tiff" << "image/x-xcf" << "image/x-xcf-gimp" << "image/x-vnd.adobe.photoshop" << "image/x-pcx" << "image/x-exr" << "image/x-portable-pixmap";

    QMimeDatabase db;
    QStringList allExtensions;
    foreach(const QString & mimeType, mimeTypes) {
        QMimeType mime = db.mimeTypeForName(mimeType);
        if (mime.isValid()) {
            allExtensions.append(mime.globPatterns());
        }
    }
    allExtensions.removeDuplicates();
    return allExtensions;
}

void ProjectList::slotRemoveInvalidClip(const QString &id, bool replace)
{
    ProjectItem *item = getItemById(id);
    m_thumbnailQueue.removeAll(id);
    if (item) {
        item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsDragEnabled | Qt::ItemIsEnabled | Qt::ItemIsEditable | Qt::ItemIsDropEnabled);
        const QString path = item->referencedClip()->fileURL().path();
        if (item->referencedClip()->isPlaceHolder()) replace = false;
        if (!path.isEmpty()) {
            if (m_invalidClipDialog) {
                m_invalidClipDialog->addClip(id, path);
                return;
            }
            else {
                if (replace)
                    m_invalidClipDialog = new InvalidDialog(i18n("Invalid clip"),  i18n("Clip is invalid, will be removed from project."), replace, QApplication::activeWindow());
                else {
                    m_invalidClipDialog = new InvalidDialog(i18n("Invalid clip"),  i18n("Clip is missing or invalid. Remove it from project?"), replace, QApplication::activeWindow());
                }
                m_invalidClipDialog->addClip(id, path);
                int result = m_invalidClipDialog->exec();
                if (result == QDialog::Accepted) replace = true;
            }
        }
        if (m_invalidClipDialog) {
            if (replace)
                emit deleteProjectClips(m_invalidClipDialog->getIds(), QMap <QString, QString>());
            delete m_invalidClipDialog;
            m_invalidClipDialog = NULL;
        }
        
    }
}

void ProjectList::slotRemoveInvalidProxy(const QString &id, bool durationError)
{
    ProjectItem *item = getItemById(id);
    if (item) {
        //qDebug()<<"// Proxy for clip "<<id<<" is invalid, delete";
        item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsDragEnabled | Qt::ItemIsEnabled | Qt::ItemIsEditable | Qt::ItemIsDropEnabled);
        if (durationError) {
            //qDebug() << "Proxy duration is wrong, try changing transcoding parameters.";
            emit displayMessage(i18n("Proxy clip unusable (duration is different from original)."), -2, ErrorMessage);
        }
        slotUpdateJobStatus(item, AbstractClipJob::PROXYJOB, JobCrashed, i18n("Failed to create proxy for %1. check parameters", item->text(0)), "project_settings");
        QString path = item->referencedClip()->getProperty("proxy");
        QUrl proxyFolder(m_doc->projectFolder().path() + QDir::separator() + "proxy/");

        //Security check: make sure the invalid proxy file is in the proxy folder
        if (proxyFolder.isParentOf(QUrl(path))) {
            QFile::remove(path);
        }
        //if (item->referencedClip()->getProducer() == NULL) {
        if (!pCore->binController()->hasClip(item->clipId())) {
            // Clip has no valid producer, request it
            slotProxyCurrentItem(false, item);
        }
        else {
            // refresh thumbs producer
            item->referencedClip()->reloadThumbProducer();
        }
    }
    m_thumbnailQueue.removeAll(id);
}

QStringList ProjectList::getGroup() const
{
    QStringList result;
    QTreeWidgetItem *item = m_listView->currentItem();
    while (item && item->type() != ProjectFoldeType)
        item = item->parent();

    if (item) {
        FolderProjectItem *folder = static_cast <FolderProjectItem *>(item);
        result << folder->clipId();
    } else {
        result << QString();
    }
    return result;
}

void ProjectList::setDocument(KdenliveDoc *doc)
{
    m_listView->blockSignals(true);
    m_abortAllJobs = true;
    for (int i = 0; i < m_jobList.count(); ++i) {
        m_jobList.at(i)->setStatus(JobAborted);
    }
    m_closing = true;
    m_jobThreads.waitForFinished();
    m_jobThreads.clearFutures();
    m_thumbnailQueue.clear();
    m_listView->clear();
    
    m_listView->setSortingEnabled(false);
    emit clipSelected(NULL);
    m_refreshed = false;
    m_allClipsProcessed = false;
    m_fps = doc->fps();
    m_timecode = doc->timecode();
    m_commandStack = doc->commandStack();
    m_doc = doc;
    m_abortAllJobs = false;
    m_closing = false;

    QMap <QString, QString> flist = doc->clipManager()->documentFolderList();
    QStringList openedFolders = doc->getExpandedFolders();
    QMapIterator<QString, QString> f(flist);
    while (f.hasNext()) {
        f.next();
        FolderProjectItem *folder = new FolderProjectItem(m_listView, QStringList() << f.value(), f.key());
        folder->setExpanded(openedFolders.contains(f.key()));
    }

    /*QList <DocClipBase*> list = doc->clipManager()->documentClipList();
    if (list.isEmpty()) {
        // blank document
        m_refreshed = true;
        m_allClipsProcessed = true;
    }
    for (int i = 0; i < list.count(); ++i)
        slotAddClip(list.at(i), false);*/

    m_listView->blockSignals(false);
    connect(m_doc->clipManager(), SIGNAL(reloadClip(QString)), this, SLOT(slotReloadClip(QString)));
    connect(m_doc->clipManager(), SIGNAL(modifiedClip(QString)), this, SLOT(slotModifiedClip(QString)));
    connect(m_doc->clipManager(), SIGNAL(missingClip(QString)), this, SLOT(slotMissingClip(QString)));
    connect(m_doc->clipManager(), SIGNAL(availableClip(QString)), this, SLOT(slotAvailableClip(QString)));
    connect(m_doc->clipManager(), SIGNAL(checkAllClips(bool,bool,QStringList)), this, SLOT(updateAllClips(bool,bool,QStringList)));
    connect(m_doc->clipManager(), SIGNAL(thumbReady(QString,int,QImage)), this, SLOT(slotSetThumbnail(QString,int,QImage)));
}

void ProjectList::slotSetThumbnail(const QString &id, int framePos, QImage img)
{
    QString fullid = id + '#' + QString::number(framePos);
    ProjectItem *pItem = NULL;
    QTreeWidgetItem *item = getAnyItemById(fullid);
    if (item && item->parent()) pItem = static_cast <ProjectItem *>(item->parent());
    if (!item && framePos == 0) pItem = getItemById(id);
    if (!item && !pItem) return;
    if (item) {
        if (item->type() == ProjectClipType) static_cast<ProjectItem*>(item)->setPixmap(QPixmap::fromImage(img));
        else item->setData(0, Qt::DecorationRole, QPixmap::fromImage(img));
    }
    else if (pItem) pItem->setPixmap(QPixmap::fromImage(img));
    if (pItem) {
        QString hash = pItem->getClipHash();
        if (!hash.isEmpty()) m_doc->cacheImage(hash + '#' + QString::number(framePos), img);
    }
}

void ProjectList::slotCheckForEmptyQueue()
{
    if (m_render->processingItems() == 0 && m_thumbnailQueue.isEmpty()) {
        if (!m_refreshed && m_allClipsProcessed) {
            m_refreshed = true;
            m_listView->setEnabled(true);
            slotClipSelected();
            QTimer::singleShot(500, this, SIGNAL(loadingIsOver()));
            emit displayMessage(QString(), -1);
        }
        updateButtons();
    } else if (!m_refreshed) {
        QTimer::singleShot(300, this, SLOT(slotCheckForEmptyQueue()));
    }
}


void ProjectList::requestClipThumbnail(const QString &id)
{
    if (!m_thumbnailQueue.contains(id)) m_thumbnailQueue.append(id);
    slotProcessNextThumbnail();
}

void ProjectList::resetThumbsProducer(DocClipBase *clip)
{
    if (!clip) return;
    clip->clearThumbProducer();
    QString id = clip->getId();
    m_thumbnailQueue.removeAll(id);
}

void ProjectList::slotProcessNextThumbnail()
{
    if (m_render->processingItems() > 0) {
        return;
    }
    if (m_thumbnailQueue.isEmpty()) {
        slotCheckForEmptyQueue();
        return;
    }
    //TODO
    /*int max = m_doc->clipManager()->clipsCount();
    emit displayMessage(i18n("Loading thumbnails"), (int)(100 *(max - m_thumbnailQueue.count()) / max));*/
    slotRefreshClipThumbnail(m_thumbnailQueue.takeFirst(), false);
}

void ProjectList::slotRefreshClipThumbnail(const QString &clipId, bool update)
{
    QTreeWidgetItem *item = getAnyItemById(clipId);
    if (item)
        slotRefreshClipThumbnail(item, update);
    else {
        slotProcessNextThumbnail();
    }
}

void ProjectList::slotRefreshClipThumbnail(QTreeWidgetItem *it, bool update)
{
    if (it == NULL) return;
    ProjectItem *item = NULL;
    bool isSubItem = false;
    int frame;
    if (it->type() == ProjectFoldeType) return;
    if (it->type() == ProjectSubclipType) {
        item = static_cast <ProjectItem *>(it->parent());
        frame = static_cast <SubProjectItem *>(it)->zone().x();
        isSubItem = true;
    } else {
        item = static_cast <ProjectItem *>(it);
        frame = item->referencedClip()->getClipThumbFrame();
    }

    if (item) {
        DocClipBase *clip = item->referencedClip();
        if (!clip) {
            slotProcessNextThumbnail();
            return;
        }
        QImage img;
        int height = m_listView->iconSize().height();
        int dwidth = (int)(height  * m_render->dar() + 0.5);
        if (clip->clipType() != Audio) {
            img = item->referencedClip()->extractImage(frame, dwidth, height);
        }
        if (!img.isNull()) {
            monitorItemEditing(false);
            QPixmap pix = roundedPixmap(img);
            processThumbOverlays(item, pix);
            if (isSubItem) it->setData(0, Qt::DecorationRole, pix);
            else item->setPixmap(pix);
            monitorItemEditing(true);
            
            QString hash = item->getClipHash();
            if (!hash.isEmpty() && !img.isNull()) {
                if (!isSubItem)
                    m_doc->cacheImage(hash, img);
                else
                    m_doc->cacheImage(hash + '#' + QString::number(frame), img);
            }
        }
        if (update)
            emit projectModified();
        slotProcessNextThumbnail();
    }
}

void ProjectList::extractMetadata(DocClipBase *clip)
{
    ClipType t = clip->clipType();
    if (t != AV && t != Video) {
        // Currently, we only use exiftool on video files
        return;
    }
    QMap <QString, QString> props = clip->properties();
    if (KdenliveSettings::use_exiftool() && !props.contains("exiftool")) {
        QMap <QString, QString> meta;
        QString url = clip->fileURL().path();
        //Check for Canon THM file
        url = url.section('.', 0, -2) + ".THM";
        if (QFile::exists(url)) {
            // Read the exif metadata embedded in the THM file
            QProcess p;
            QStringList args;
            args << "-g" << "-args" << url;
            p.start("exiftool", args);
            p.waitForFinished();
            QString res = p.readAllStandardOutput();
            QStringList list = res.split('\n');
            foreach(const QString &tagline, list) {
                if (tagline.startsWith(QLatin1String("-File")) || tagline.startsWith(QLatin1String("-ExifTool"))) continue;
                QString tag = tagline.section(':', 1).simplified();
                if (tag.startsWith(QLatin1String("ImageWidth")) || tag.startsWith(QLatin1String("ImageHeight"))) continue;
                if (!tag.section('=', 0, 0).isEmpty() && !tag.section('=', 1).simplified().isEmpty())
                    meta.insert(tag.section('=', 0, 0), tag.section('=', 1).simplified());
            }
        } else {
            QString codecid = props.value("videocodecid").simplified();
            if (codecid == "h264") {
                QProcess p;
                QStringList args;
                args << "-g" << "-args" << clip->fileURL().path();
                p.start("exiftool", args);
                p.waitForFinished();
                QString res = p.readAllStandardOutput();
                QStringList list = res.split('\n');
                foreach(const QString &tagline, list) {
                    if (!tagline.startsWith(QLatin1String("-H264"))) continue;
                    QString tag = tagline.section(':', 1);
                    if (tag.startsWith(QLatin1String("ImageWidth")) || tag.startsWith(QLatin1String("ImageHeight"))) continue;
                    meta.insert(tag.section('=', 0, 0), tag.section('=', 1));
                }
            }
        }
        clip->setProperty("exiftool", "1");
        if (!meta.isEmpty()) {
            clip->setMetadata(meta, "ExifTool");
            //checkCamcorderFilters(clip, meta);
        }
    }
    if (KdenliveSettings::use_magicLantern() && !props.contains("magiclantern")) {
        QMap <QString, QString> meta;
        QString url = clip->fileURL().path();
        url = url.section('.', 0, -2) + ".LOG";
        if (QFile::exists(url)) {
            QFile file(url);
            if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
                while (!file.atEnd()) {
                    QString line = file.readLine().simplified();
                    if (line.startsWith('#') || line.isEmpty() || !line.contains(':')) continue;
                    if (line.startsWith(QLatin1String("CSV data"))) break;
                    meta.insert(line.section(':', 0, 0).simplified(), line.section(':', 1).simplified());
                }
            }
        }

        if (!meta.isEmpty())
            clip->setMetadata(meta, "Magic Lantern");
        clip->setProperty("magiclantern", "1");
    }
}

bool ProjectList::adjustProjectProfileToItem(ProjectItem *item)
{
    if (item == NULL) {
        if (m_listView->currentItem() && m_listView->currentItem()->type() != ProjectFoldeType)
            item = static_cast <ProjectItem*>(m_listView->currentItem());
    }
    if (item == NULL || item->referencedClip() == NULL) {
        KMessageBox::information(QApplication::activeWindow(), i18n("Cannot find profile from current clip"));
        return false;
    }
    bool profileUpdated = false;
    QString size = item->referencedClip()->getProperty("frame_size");
    int width = size.section('x', 0, 0).toInt();
    int height = size.section('x', -1).toInt();
    // Fix some avchd clips tht report a wrong size (1920x1088)
    if (height == 1088) height = 1080;
    double fps = item->referencedClip()->getProperty("fps").toDouble();
    double par = item->referencedClip()->getProperty("aspect_ratio").toDouble();
    if (item->clipType() == Image || item->clipType() == AV || item->clipType() == Video) {
        if (ProfilesDialog::matchProfile(width, height, fps, par, item->clipType() == Image, m_doc->mltProfile()) == false) {
            // get a list of compatible profiles
            QMap <QString, QString> suggestedProfiles = ProfilesDialog::getProfilesFromProperties(width, height, fps, par, item->clipType() == Image);
            if (!suggestedProfiles.isEmpty()) {
                QPointer<QDialog> dialog = new QDialog(this);
                dialog->setWindowTitle(i18n("Change project profile"));
                QDialogButtonBox *buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok|QDialogButtonBox::Cancel);
                QWidget *mainWidget = new QWidget(this);
                QVBoxLayout *l = new QVBoxLayout;
                QLabel *label = new QLabel(i18n("Your clip does not match current project's profile.\nDo you want to change the project profile?\n\nThe following profiles match the clip (size: %1, fps: %2)", size, fps), dialog);
                l->addWidget(label);
                QListWidget *list = new QListWidget(dialog);
                list->setAlternatingRowColors(true);
                QMapIterator<QString, QString> i(suggestedProfiles);
                while (i.hasNext()) {
                    i.next();
                    QListWidgetItem *item = new QListWidgetItem(i.value(), list);
                    item->setData(Qt::UserRole, i.key());
                    item->setToolTip(i.key());
                }
                list->setCurrentRow(0);
                l->addWidget(list);
                mainWidget->setLayout(l);
                QVBoxLayout *mainLayout = new QVBoxLayout;
                dialog->setLayout(mainLayout);
                mainLayout->addWidget(mainWidget);
                QPushButton *okButton = buttonBox->button(QDialogButtonBox::Ok);
                okButton->setText(i18n("Update profile"));
                okButton->setDefault(true);
                okButton->setShortcut(Qt::CTRL | Qt::Key_Return);
                dialog->connect(buttonBox, SIGNAL(accepted()), dialog, SLOT(accept()));
                dialog->connect(buttonBox, SIGNAL(rejected()), dialog, SLOT(reject()));
                mainLayout->addWidget(buttonBox);
                
                if (dialog->exec() == QDialog::Accepted) {
                    //Change project profile
                    profileUpdated = true;
                    if (list->currentItem())
                        emit updateProfile(list->currentItem()->data(Qt::UserRole).toString());
                }
                dialog->deleteLater();
            } else if (fps > 0) {
                KMessageBox::information(QApplication::activeWindow(), i18n("Your clip does not match current project's profile.\nNo existing profile found to match the clip's properties.\nClip size: %1\nFps: %2\n", size, fps));
            }
        }
    }
    return profileUpdated;
}

QString ProjectList::getDocumentProperty(const QString &key) const
{
    return m_doc->getDocumentProperty(key);
}

bool ProjectList::useProxy() const
{
    return m_doc->getDocumentProperty("enableproxy").toInt();
}

bool ProjectList::generateProxy() const
{
    return m_doc->getDocumentProperty("generateproxy").toInt();
}

bool ProjectList::generateImageProxy() const
{
    return m_doc->getDocumentProperty("generateimageproxy").toInt();
}

QTreeWidgetItem *ProjectList::getAnyItemById(const QString &id)
{
    QTreeWidgetItemIterator it(m_listView);
    QString lookId = id;
    if (id.contains('#'))
        lookId = id.section('#', 0, 0);

    ProjectItem *result = NULL;
    while (*it) {
        if ((*it)->type() != ProjectClipType) {
            // subitem
            ++it;
            continue;
        }
        ProjectItem *item = static_cast<ProjectItem *>(*it);
        if (item->clipId() == lookId) {
            result = item;
            break;
        }
        ++it;
    }
    if (result == NULL || !id.contains('#')) {
        return result;
    } else {
        for (int i = 0; i < result->childCount(); ++i) {
            SubProjectItem *sub = static_cast <SubProjectItem *>(result->child(i));
            if (sub && sub->zone().x() == id.section('#', 1, 1).toInt())
                return sub;
        }
    }

    return NULL;
}


ProjectItem *ProjectList::getItemById(const QString &id)
{
    ProjectItem *item;
    QTreeWidgetItemIterator it(m_listView);
    while (*it) {
        if ((*it)->type() != ProjectClipType) {
            // subitem or folder
            ++it;
            continue;
        }
        item = static_cast<ProjectItem *>(*it);
        if (item->clipId() == id)
            return item;
        ++it;
    }
    return NULL;
}

FolderProjectItem *ProjectList::getFolderItemById(const QString &id)
{
    FolderProjectItem *item;
    QTreeWidgetItemIterator it(m_listView);
    while (*it) {
        if ((*it)->type() == ProjectFoldeType) {
            item = static_cast<FolderProjectItem *>(*it);
            if (item->clipId() == id)
                return item;
        }
        ++it;
    }
    return NULL;
}

void ProjectList::regenerateTemplate(const QString &id)
{
    ProjectItem *clip = getItemById(id);
    if (clip)
        regenerateTemplate(clip);
}

void ProjectList::regenerateTemplate(ProjectItem *clip)
{
    //TODO: remove this unused method, only force_reload is necessary
    clip->referencedClip()->getProducer()->set("force_reload", 1);
}

void ProjectList::slotAddClipCut(const QString &id, int in, int out)
{
    ProjectItem *clip = getItemById(id);
    if (clip == NULL || clip->referencedClip()->hasCutZone(QPoint(in, out)))
        return;
    AddClipCutCommand *command = new AddClipCutCommand(this, id, in, out, QString(), true, false);
    m_commandStack->push(command);
}

void ProjectList::addClipCut(const QString &id, int in, int out, const QString desc, bool newItem)
{
    ProjectItem *clip = getItemById(id);
    if (clip) {
        DocClipBase *base = clip->referencedClip();
        base->addCutZone(in, out);
        monitorItemEditing(false);
        SubProjectItem *sub = new SubProjectItem(m_render->dar(), clip, in, out, desc);
        if (newItem && desc.isEmpty() && !m_listView->isColumnHidden(1)) {
            if (!clip->isExpanded())
                clip->setExpanded(true);
            m_listView->scrollToItem(sub);
            m_listView->editItem(sub, 1);
        }
        //m_doc->clipManager()->slotRequestThumbs(QString('#' + id), QList <int>() << in);
        monitorItemEditing(true);
    }
    emit projectModified();
}

void ProjectList::removeClipCut(const QString &id, int in, int out)
{
    ProjectItem *clip = getItemById(id);
    if (clip) {
        DocClipBase *base = clip->referencedClip();
        base->removeCutZone(in, out);
        SubProjectItem *sub = getSubItem(clip, QPoint(in, out));
        if (sub) {
            monitorItemEditing(false);
            delete sub;
            monitorItemEditing(true);
        }
    }
    emit projectModified();
}

SubProjectItem *ProjectList::getSubItem(ProjectItem *clip, const QPoint &zone)
{
    SubProjectItem *sub = NULL;
    if (clip) {
        for (int i = 0; i < clip->childCount(); ++i) {
            QTreeWidgetItem *it = clip->child(i);
            if (it->type() == ProjectSubclipType) {
                sub = static_cast <SubProjectItem*>(it);
                if (sub->zone() == zone)
                    break;
                else
                    sub = NULL;
            }
        }
    }
    return sub;
}

void ProjectList::slotUpdateClipCut(QPoint p)
{
    if (!m_listView->currentItem() || m_listView->currentItem()->type() != ProjectSubclipType)
        return;
    SubProjectItem *sub = static_cast <SubProjectItem*>(m_listView->currentItem());
    ProjectItem *item = static_cast <ProjectItem *>(sub->parent());
    EditClipCutCommand *command = new EditClipCutCommand(this, item->clipId(), sub->zone(), p, sub->text(1), sub->text(1), true);
    m_commandStack->push(command);
}

void ProjectList::doUpdateClipCut(const QString &id, const QPoint &oldzone, const QPoint &zone, const QString &comment)
{
    ProjectItem *clip = getItemById(id);
    SubProjectItem *sub = getSubItem(clip, oldzone);
    if (sub == NULL || clip == NULL)
        return;
    DocClipBase *base = clip->referencedClip();
    base->updateCutZone(oldzone.x(), oldzone.y(), zone.x(), zone.y(), comment);
    monitorItemEditing(false);
    sub->setZone(zone);
    sub->setDescription(comment);
    monitorItemEditing(true);
    emit projectModified();
}

void ProjectList::slotAddOrUpdateSequence(const QString &frameName)
{
    QString fileName = QUrl(frameName).fileName().section('_', 0, -2);
    QStringList list;
    QString pattern = SlideshowClip::selectedPath(QUrl::fromLocalFile(frameName), false, QString(), &list);
    int count = list.count();
    if (count > 1) {
        //TODO
        /*
        const QList <DocClipBase *> existing = m_doc->clipManager()->getClipByResource(pattern);
        if (!existing.isEmpty()) {
            // Sequence already exists, update
            QString id = existing.at(0)->getId();
            //ProjectItem *item = getItemById(id);
            QMap <QString, QString> oldprops;
            QMap <QString, QString> newprops;
            int ttl = existing.at(0)->getProperty("ttl").toInt();
            oldprops["out"] = existing.at(0)->getProperty("out");
            newprops["out"] = QString::number(ttl * count - 1);
            slotUpdateClipProperties(id, newprops);
            /*EditClipCommand *command = new EditClipCommand(this, id, oldprops, newprops, false);
            m_commandStack->push(command);
        } else {
            // Create sequence
            QStringList groupInfo = getGroup();
            QMap <QString, QString> properties;
            properties.insert("name", fileName);
            properties.insert("resource", pattern);
            properties.insert("in", "0");
            QString duration = m_timecode.reformatSeparators(KdenliveSettings::sequence_duration());
            properties.insert("out", QString::number(m_doc->getFramePos(duration) * count));
            properties.insert("ttl", QString::number(m_doc->getFramePos(duration)));
            properties.insert("loop", QString::number(false));
            properties.insert("crop", QString::number(false));
            properties.insert("fade", QString::number(false));
            properties.insert("luma_duration", m_timecode.getTimecodeFromFrames(int(ceil(m_timecode.fps()))));

            m_doc->slotCreateSlideshowClipFile(properties, groupInfo.at(0), groupInfo.at(1));
        }
        */
    } else emit displayMessage(i18n("Sequence not found"), -2, ErrorMessage);
}

QMap <QString, QString> ProjectList::getProxies()
{
    QMap <QString, QString> list;
    ProjectItem *item;
    QTreeWidgetItemIterator it(m_listView);
    while (*it) {
        if ((*it)->type() != ProjectClipType) {
            ++it;
            continue;
        }
        item = static_cast<ProjectItem *>(*it);
        if (item && item->referencedClip() != NULL) {
            if (item->hasProxy()) {
                QString proxy = item->referencedClip()->getProperty("proxy");
                list.insert(proxy, item->clipUrl().path());
            }
        }
        ++it;
    }
    return list;
}

void ProjectList::slotCreateProxy(const QString &id)
{
    ProjectItem *item = getItemById(id);
    if (!item || hasPendingJob(item, AbstractClipJob::PROXYJOB) || item->referencedClip()->isPlaceHolder()) return;
    QString path = item->referencedClip()->getProperty("proxy");
    if (path.isEmpty()) {
        slotUpdateJobStatus(item, AbstractClipJob::PROXYJOB, JobCrashed, i18n("Failed to create proxy, empty path."));
        return;
    }
    
    if (QFileInfo(path).size() > 0) {
        // Proxy already created
        setJobStatus(item, AbstractClipJob::PROXYJOB, JobDone);
        slotGotProxy(path);
        return;
    }
    QString sourcePath = item->clipUrl().path();
    if (item->clipType() == Playlist) {
        // Special case: playlists use the special 'consumer' producer to support resizing
        sourcePath.prepend("consumer:");
    }
    ProxyJob *job = new ProxyJob(item->clipType(), id, QStringList() << path << sourcePath << item->referencedClip()->producerProperty("_exif_orientation") << m_doc->getDocumentProperty("proxyparams").simplified() << QString::number(m_render->frameRenderWidth()) << QString::number(m_render->renderHeight()));
    if (job->isExclusive() && hasPendingJob(item, job->jobType)) {
        delete job;
        return;
    }

    m_jobList.append(job);
    setJobStatus(item, job->jobType, JobWaiting, 0, job->statusMessage());
    slotCheckJobProcess();
}

void ProjectList::slotCheckJobProcess()
{        
    if (!m_jobThreads.futures().isEmpty()) {
        // Remove inactive threads
        QList <QFuture<void> > futures = m_jobThreads.futures();
        m_jobThreads.clearFutures();
        for (int i = 0; i < futures.count(); ++i)
            if (!futures.at(i).isFinished()) {
                m_jobThreads.addFuture(futures.at(i));
            }
    }
    if (m_jobList.isEmpty()) return;

    m_jobMutex.lock();
    int count = 0;
    for (int i = 0; i < m_jobList.count(); ++i) {
        if (m_jobList.at(i)->status() == JobWorking || m_jobList.at(i)->status() == JobWaiting)
            count ++;
        else {
            // remove finished jobs
            AbstractClipJob *job = m_jobList.takeAt(i);
            job->deleteLater();
            --i;
        }
    }
    emit jobCount(count);
    m_jobMutex.unlock();
    if (m_jobThreads.futures().isEmpty() || m_jobThreads.futures().count() < KdenliveSettings::proxythreads()) m_jobThreads.addFuture(QtConcurrent::run(this, &ProjectList::slotProcessJobs));
}

void ProjectList::slotProcessJobs()
{
  /*
    while (!m_jobList.isEmpty() && !m_abortAllJobs) {
        emit projectModified();
        AbstractClipJob *job = NULL;
        int count = 0;
        m_jobMutex.lock();
        for (int i = 0; i < m_jobList.count(); ++i) {
            if (m_jobList.at(i)->status() == JobWaiting) {
                if (job == NULL) {
                    m_jobList.at(i)->setStatus(JobWorking);
                    job = m_jobList.at(i);
                }
                count++;
            }
            else if (m_jobList.at(i)->status() == JobWorking)
                count ++;
        }
        // Set jobs count
        emit jobCount(count);
        m_jobMutex.unlock();

        if (job == NULL) {
            break;
        }
        QString destination = job->destination();
        // Check if the clip is still here
        DocClipBase *currentClip = m_doc->clipManager()->getClipById(job->clipId());
        //ProjectItem *processingItem = getItemById(job->clipId());
        if (currentClip == NULL) {
            job->setStatus(JobDone);
            continue;
        }
        // Set clip status to started
        emit processLog(job->clipId(), 0, job->jobType, job->statusMessage());

        // Make sure destination path is writable
        if (!destination.isEmpty()) {
            QFile file(destination);
            if (!file.open(QIODevice::WriteOnly)) {
                emit updateJobStatus(job->clipId(), job->jobType, JobCrashed, i18n("Cannot write to path: %1", destination));
                job->setStatus(JobCrashed);
                continue;
            }
            file.close();
            QFile::remove(destination);
        }
        connect(job, SIGNAL(jobProgress(QString,int,int)), this, SIGNAL(processLog(QString,int,int)));
        connect(job, SIGNAL(cancelRunningJob(QString,stringMap)), this, SIGNAL(cancelRunningJob(QString,stringMap)));

        if (job->jobType == AbstractClipJob::MLTJOB) {
            MeltJob *jb = static_cast<MeltJob *> (job);
            //jb->setProducer(currentClip->getProducer(), currentClip->fileURL());
	    jb->setProducer(pCore->binController()->getBinClip(currentClip->getId(), -1), currentClip->fileURL());
            if (jb->isProjectFilter())
                connect(job, SIGNAL(gotFilterJobResults(QString,int,int,stringMap,stringMap)), this, SLOT(slotGotFilterJobResults(QString,int,int,stringMap,stringMap)));
            else
                connect(job, SIGNAL(gotFilterJobResults(QString,int,int,stringMap,stringMap)), this, SIGNAL(gotFilterJobResults(QString,int,int,stringMap,stringMap)));
        }
        job->startJob();
        if (job->status() == JobDone) {
            emit updateJobStatus(job->clipId(), job->jobType, JobDone);
            //TODO: replace with more generic clip replacement framework
            if (job->jobType == AbstractClipJob::PROXYJOB) emit gotProxy(job->clipId());
            if (job->addClipToProject()) {
                emit addClip(destination, QString(), QString());
            }
        } else if (job->status() == JobCrashed || job->status() == JobAborted) {
            emit updateJobStatus(job->clipId(), job->jobType, job->status(), job->errorMessage(), QString(), job->logDetails());
        }
    }
    // Thread finished, cleanup & update count
    QTimer::singleShot(200, this, SIGNAL(checkJobProcess()));
    */
}


void ProjectList::updateProxyConfig()
{
    ProjectItem *item;
    QTreeWidgetItemIterator it(m_listView);
    QUndoCommand *command = new QUndoCommand();
    command->setText(i18n("Update proxy settings"));
    QString proxydir = m_doc->projectFolder().path() + QDir::separator() + "proxy/";
    while (*it) {
        if ((*it)->type() != ProjectClipType) {
            ++it;
            continue;
        }
        item = static_cast<ProjectItem *>(*it);
        if (item == NULL) {
            ++it;
            continue;
        }
        ClipType t = item->clipType();
        if ((t == Video || t == AV || t == Unknown) && item->referencedClip() != NULL) {
            if  (generateProxy() && useProxy() && !hasPendingJob(item, AbstractClipJob::PROXYJOB)) {
                DocClipBase *clip = item->referencedClip();
                if (clip->getProperty("frame_size").section('x', 0, 0).toInt() > m_doc->getDocumentProperty("proxyminsize").toInt()) {
                    if (clip->getProperty("proxy").isEmpty()) {
                        // We need to insert empty proxy in old properties so that undo will work
                        QMap <QString, QString> oldProps;// = clip->properties();
                        oldProps.insert("proxy", QString());
                        QMap <QString, QString> newProps;
                        newProps.insert("proxy", proxydir + item->referencedClip()->getClipHash() + '.' + m_doc->getDocumentProperty("proxyextension"));
                        //TODO
			//new EditClipCommand(this, clip->getId(), oldProps, newProps, true, command);
                    }
                }
            }
            else if (item->hasProxy()) {
                // remove proxy
                QMap <QString, QString> newProps;
                newProps.insert("proxy", QString());
                // insert required duration for proxy
                newProps.insert("proxy_out", item->referencedClip()->producerProperty("out"));
                //TODO
		// new EditClipCommand(this, item->clipId(), item->referencedClip()->currentProperties(newProps), newProps, true, command);
            }
        }
        else if (t == Image && item->referencedClip() != NULL) {
            if  (generateImageProxy() && useProxy()) {
                DocClipBase *clip = item->referencedClip();
                int maxImageSize = m_doc->getDocumentProperty("proxyimageminsize").toInt();
                if (clip->getProperty("frame_size").section('x', 0, 0).toInt() > maxImageSize || clip->getProperty("frame_size").section('x', 1, 1).toInt() > maxImageSize) {
                    if (clip->getProperty("proxy").isEmpty()) {
                        // We need to insert empty proxy in old properties so that undo will work
                        QMap <QString, QString> oldProps = clip->properties();
                        oldProps.insert("proxy", QString());
                        QMap <QString, QString> newProps;
                        newProps.insert("proxy", proxydir + item->referencedClip()->getClipHash() + ".png");
                        //TODO
			new EditClipCommand(pCore->bin(), clip->getId(), oldProps, newProps, true, command);
                    }
                }
            }
            else if (item->hasProxy()) {
                // remove proxy
                QMap <QString, QString> newProps;
                newProps.insert("proxy", QString());
                new EditClipCommand(pCore->bin(), item->clipId(), item->referencedClip()->properties(), newProps, true, command);
            }
        }
        ++it;
    }
    if (command->childCount() > 0) m_doc->commandStack()->push(command);
    else delete command;
}

void ProjectList::slotProcessLog(const QString &id, int progress, int type, const QString &message)
{
    ProjectItem *item = getItemById(id);
    setJobStatus(item, (AbstractClipJob::JOBTYPE) type, JobWorking, progress, message);
}

void ProjectList::slotProxyCurrentItem(bool doProxy, ProjectItem *itemToProxy)
{
    QList<QTreeWidgetItem *> list;
    if (itemToProxy == NULL) list = m_listView->selectedItems();
    else list << itemToProxy;

    // expand list (folders, subclips) to get real clips
    QTreeWidgetItem *listItem;
    QList<ProjectItem *> clipList;
    for (int i = 0; i < list.count(); ++i) {
        listItem = list.at(i);
        if (listItem->type() == ProjectFoldeType) {
            for (int j = 0; j < listItem->childCount(); ++j) {
                QTreeWidgetItem *sub = listItem->child(j);
                if (sub->type() == ProjectClipType) {
                    ProjectItem *item = static_cast <ProjectItem*>(sub);
                    if (!clipList.contains(item)) clipList.append(item);
                }
            }
        }
        else if (listItem->type() == ProjectSubclipType) {
            QTreeWidgetItem *sub = listItem->parent();
            ProjectItem *item = static_cast <ProjectItem*>(sub);
            if (!clipList.contains(item)) clipList.append(item);
        }
        else if (listItem->type() == ProjectClipType) {
            ProjectItem *item = static_cast <ProjectItem*>(listItem);
            if (!clipList.contains(item)) clipList.append(item);
        }
    }
    
    QUndoCommand *command = new QUndoCommand();
    if (doProxy) command->setText(i18np("Add proxy clip", "Add proxy clips", clipList.count()));
    else command->setText(i18np("Remove proxy clip", "Remove proxy clips", clipList.count()));
    
    // Make sure the proxy folder exists
    QString proxydir = m_doc->projectFolder().path() + QDir::separator() + "proxy/";
    QDir dir(m_doc->projectFolder().path());
    dir.mkdir("proxy");

    QMap <QString, QString> newProps;
    QMap <QString, QString> oldProps;
    if (!doProxy) newProps.insert("proxy", "-");
    for (int i = 0; i < clipList.count(); ++i) {
        ProjectItem *item = clipList.at(i);
        ClipType t = item->clipType();
        if ((t == Video || t == AV || t == Unknown || t == Image || t == Playlist) && item->referencedClip()) {
            //if ((doProxy && item->hasProxy()) || (!doProxy && !item->hasProxy() && item->referencedClip()->getProducer() != NULL)) continue;
	    if ((doProxy && item->hasProxy()) || (!doProxy && !item->hasProxy() && pCore->binController()->hasClip(item->clipId()))) continue;
            DocClipBase *clip = item->referencedClip();
            if (!clip || !clip->isClean() || m_render->isProcessing(item->clipId())) {
                //qDebug()<<"//// TRYING TO PROXY: "<<item->clipId()<<", but it is busy";
                continue;
            }

            //oldProps = clip->properties();
            if (doProxy) {
                newProps.clear();
                QString path = proxydir + clip->getClipHash() + '.' + (t == Image ? "png" : m_doc->getDocumentProperty("proxyextension"));
                // insert required duration for proxy
                newProps.insert("proxy_out", clip->producerProperty("out"));
                newProps.insert("proxy", path);
                // We need to insert empty proxy so that undo will work
                //oldProps.insert("proxy", QString());
            }
            //else if (item->referencedClip()->getProducer() == NULL) {
            else if (!pCore->binController()->hasClip(item->clipId())) {
                // Force clip reload
                //qDebug()<<"// CLIP HAD NULL PROD------------";
                newProps.insert("resource", item->referencedClip()->getProperty("resource"));
            }
            // We need to insert empty proxy so that undo will work
            oldProps = clip->currentProperties(newProps);
            if (doProxy) oldProps.insert("proxy", "-");
            //TODO
	    //new EditClipCommand(this, item->clipId(), oldProps, newProps, true, command);
        }
    }
    if (command->childCount() > 0) {
        m_doc->commandStack()->push(command);
    }
    else delete command;
}


void ProjectList::slotDeleteProxy(const QString proxyPath)
{
    if (proxyPath.isEmpty()) return;
    QUndoCommand *proxyCommand = new QUndoCommand();
    proxyCommand->setText(i18n("Remove Proxy"));
    QTreeWidgetItemIterator it(m_listView);
    ProjectItem *item;
    while (*it) {
        if ((*it)->type() == ProjectClipType) {
            item = static_cast <ProjectItem *>(*it);
            if (item->referencedClip()->getProperty("proxy") == proxyPath) {
                QMap <QString, QString> props;
                props.insert("proxy", QString());
                //TODO
		//new EditClipCommand(this, item->clipId(), item->referencedClip()->currentProperties(props), props, true, proxyCommand);

            }
        }
        ++it;
    }
    if (proxyCommand->childCount() == 0)
        delete proxyCommand;
    else
        m_commandStack->push(proxyCommand);
    QFile::remove(proxyPath);
}

void ProjectList::setJobStatus(ProjectItem *item, AbstractClipJob::JOBTYPE jobType, ClipJobStatus status, int progress, const QString &statusMessage)
{
    if (item == NULL || (m_abortAllJobs && m_closing)) return;
    monitorItemEditing(false);
    item->setJobStatus(jobType, status, progress, statusMessage);
    if (status == JobCrashed) {
        DocClipBase *clip = item->referencedClip();
        if (!clip) {
            //qDebug()<<"// PROXY CRASHED";
        }
        //else if (clip->getProducer() == NULL && !clip->isPlaceHolder()) {
        else if (!pCore->binController()->hasClip(clip->getId()) && !clip->isPlaceHolder()) {
            // disable proxy and fetch real clip
            clip->setProperty("proxy", "-");
            QDomElement xml = clip->toXML();
            m_render->getFileProperties(xml, clip->getId(), m_listView->iconSize().height(), true);
        }
        else {
            // Disable proxy for this clip
            clip->setProperty("proxy", "-");
        }
    }
    monitorItemEditing(true);
}

void ProjectList::monitorItemEditing(bool enable)
{
    if (enable) connect(m_listView, SIGNAL(itemChanged(QTreeWidgetItem*,int)), this, SLOT(slotItemEdited(QTreeWidgetItem*,int)));
    else disconnect(m_listView, SIGNAL(itemChanged(QTreeWidgetItem*,int)), this, SLOT(slotItemEdited(QTreeWidgetItem*,int)));
}

QStringList ProjectList::expandedFolders() const
{
    QStringList result;
    FolderProjectItem *item;
    QTreeWidgetItemIterator it(m_listView);
    while (*it) {
        if ((*it)->type() != ProjectFoldeType) {
            ++it;
            continue;
        }
        if ((*it)->isExpanded()) {
            item = static_cast<FolderProjectItem *>(*it);
            result.append(item->clipId());
        }
        ++it;
    }
    return result;
}

void ProjectList::processThumbOverlays(ProjectItem *item, QPixmap &pix)
{
    if (item->hasProxy()) {
        QPainter p(&pix);
        QColor c(220, 220, 10, 200);
        QRect r(0, 0, 12, 12);
        p.fillRect(r, c);
        QFont font = p.font();
        font.setBold(true);
        p.setFont(font);
        p.setPen(Qt::black);
        p.drawText(r, Qt::AlignCenter, i18nc("The first letter of Proxy, used as abbreviation", "P"));
    }
}

void ProjectList::slotCancelJobs()
{
    //TODO in jobmanager
  /*
    m_abortAllJobs = true;
    for (int i = 0; i < m_jobList.count(); ++i) {
        m_jobList.at(i)->setStatus(JobAborted);
    }
    m_jobThreads.waitForFinished();
    m_jobThreads.clearFutures();
    QUndoCommand *command = new QUndoCommand();
    command->setText(i18np("Cancel job", "Cancel jobs", m_jobList.count()));
    m_jobMutex.lock();
    for (int i = 0; i < m_jobList.count(); ++i) {
        DocClipBase *currentClip = m_doc->clipManager()->getClipById(m_jobList.at(i)->clipId());
        if (!currentClip) continue;
        QMap <QString, QString> newProps = m_jobList.at(i)->cancelProperties();
        if (newProps.isEmpty()) continue;
        QMap <QString, QString> oldProps = currentClip->currentProperties(newProps);
        //TODO
	//new EditClipCommand(this, m_jobList.at(i)->clipId(), oldProps, newProps, true, command);
    }
    m_jobMutex.unlock();
    if (command->childCount() > 0) {
        m_doc->commandStack()->push(command);
    }
    else delete command;
    if (!m_jobList.isEmpty()) qDeleteAll(m_jobList);
    m_jobList.clear();
    m_abortAllJobs = false;
    m_infoLabel->slotSetJobCount(0);
    */
}

void ProjectList::slotCancelRunningJob(const QString id, stringMap newProps)
{
    //TODO
    /*
    if (newProps.isEmpty() || m_closing) return;
    DocClipBase *currentClip = m_doc->clipManager()->getClipById(id);
    if (!currentClip) return;
    QMap <QString, QString> oldProps = currentClip->currentProperties(newProps);
    if (newProps == oldProps) return;
    QMapIterator<QString, QString> i(oldProps);
    EditClipCommand *command = new EditClipCommand(this, id, oldProps, newProps, true);
    m_commandStack->push(command);
    */
}

bool ProjectList::hasPendingJob(ProjectItem *item, AbstractClipJob::JOBTYPE type)
{
    if (!item || !item->referencedClip() || m_abortAllJobs) return false;
    QMutexLocker lock(&m_jobMutex);
    for (int i = 0; i < m_jobList.count(); ++i) {
        if (m_abortAllJobs) break;
        AbstractClipJob *job = m_jobList.at(i);
        if (job->clipId() == item->clipId() && job->jobType == type && (job->status() == JobWaiting || job->status() == JobWorking)) return true;
    }
    
    return false;
}

void ProjectList::deleteJobsForClip(const QString &clipId)
{
    QMutexLocker lock(&m_jobMutex);
    for (int i = 0; i < m_jobList.count(); ++i) {
        if (m_jobList.at(i)->clipId() == clipId) {
            m_jobList.at(i)->setStatus(JobAborted);
        }
    }
}

void ProjectList::slotUpdateJobStatus(const QString id, int type, int status, const QString label, const QString actionName, const QString details)
{
    ProjectItem *item = getItemById(id);
    if (!item) return;
    slotUpdateJobStatus(item, type, status, label, actionName, details);
    
}

void ProjectList::slotUpdateJobStatus(ProjectItem *item, int type, int status, const QString &label, const QString &actionName, const QString details)
{
    item->setJobStatus((AbstractClipJob::JOBTYPE) type, (ClipJobStatus) status);
    if (status != JobCrashed) return;
    QList<QAction *> actions = m_infoMessage->actions();
    if (m_infoMessage->isHidden()) {
        m_infoMessage->setText(label);
        m_infoMessage->setWordWrap(m_infoMessage->text().length() > 35);
        m_infoMessage->setMessageType(KMessageWidget::Warning);
    }
    
    if (!actionName.isEmpty()) {
        QAction *action = NULL;
        QList< KActionCollection * > collections = KActionCollection::allCollections();
        for (int i = 0; i < collections.count(); ++i) {
            KActionCollection *coll = collections.at(i);
            action = coll->action(actionName);
            if (action) break;
        }
        if (action && !actions.contains(action)) m_infoMessage->addAction(action);
    }
    if (!details.isEmpty()) {
        m_errorLog.append(details);
        if (!actions.contains(m_logAction)) m_infoMessage->addAction(m_logAction);
    }
    m_infoMessage->animatedShow();
}

void ProjectList::slotShowJobLog()
{
    QDialog d(this);
    QDialogButtonBox *buttonBox = new QDialogButtonBox(QDialogButtonBox::Close);
    QWidget *mainWidget = new QWidget(this);
    QVBoxLayout *l = new QVBoxLayout;
    QTextEdit t(&d);
    for (int i = 0; i < m_errorLog.count(); ++i) {
        if (i > 0) t.insertHtml("<br><hr /><br>");
        t.insertPlainText(m_errorLog.at(i));
    }
    t.setReadOnly(true);
    l->addWidget(&t);
    mainWidget->setLayout(l);
    QVBoxLayout *mainLayout = new QVBoxLayout;
    d.setLayout(mainLayout);
    mainLayout->addWidget(mainWidget);
    mainLayout->addWidget(buttonBox);
    d.connect(buttonBox, SIGNAL(accepted()), &d, SLOT(accept()));
    d.exec();
}

QStringList ProjectList::getPendingJobs(const QString &id)
{
    QStringList result;
    QMutexLocker lock(&m_jobMutex);
    for (int i = 0; i < m_jobList.count(); ++i) {
        if (m_jobList.at(i)->clipId() == id && (m_jobList.at(i)->status() == JobWaiting || m_jobList.at(i)->status() == JobWorking)) {
            // discard this job
            result << m_jobList.at(i)->description;
        }
    }
    return result;
}

void ProjectList::discardJobs(const QString &id, AbstractClipJob::JOBTYPE type) {
    QMutexLocker lock(&m_jobMutex);
    for (int i = 0; i < m_jobList.count(); ++i) {
        if (m_jobList.at(i)->clipId() == id && (m_jobList.at(i)->jobType == type || type == AbstractClipJob::NOJOBTYPE)) {
            // discard this job
            m_jobList.at(i)->setStatus(JobAborted);
        }
    }
}


void ProjectList::slotPrepareJobsMenu()
{
    ProjectItem *item;
    if (!m_listView->currentItem() || m_listView->currentItem()->type() == ProjectFoldeType)
        return;
    if (m_listView->currentItem()->type() == ProjectSubclipType)
        item = static_cast <ProjectItem*>(m_listView->currentItem()->parent());
    else
        item = static_cast <ProjectItem*>(m_listView->currentItem());
    if (item && (item->flags() & Qt::ItemIsDragEnabled)) {
        QString id = item->clipId();
        m_discardCurrentClipJobs->setData(id);
        QStringList jobs = getPendingJobs(id);
        m_discardCurrentClipJobs->setEnabled(!jobs.isEmpty());
    } else {
        m_discardCurrentClipJobs->setData(QString());
        m_discardCurrentClipJobs->setEnabled(false);
    }
}

void ProjectList::slotDiscardClipJobs()
{
    QString id = m_discardCurrentClipJobs->data().toString();
    if (id.isEmpty()) return;
    discardJobs(id);
}

void ProjectList::updatePalette()
{
    m_infoLabel->setStyleSheet(SmallInfoLabel::getStyleSheet(QApplication::palette()));
    m_listView->updateStyleSheet();
}


void ProjectList::slotGotFilterJobResults(QString id, int , int , stringMap results, stringMap filterInfo)
{
    // Currently, only the first value of results is used
    ////qDebug()<<"// FILTER RES:\n"<<filterInfo<<"\n--------------\n"<<results;
    ProjectItem *clip = getItemById(id);
    if (!clip) return;

    // Check for return value
    int markersType = -1;
    if (filterInfo.contains("addmarkers")) markersType = filterInfo.value("addmarkers").toInt();
    if (results.isEmpty()) {
        emit displayMessage(i18n("No data returned from clip analysis"), 0, ErrorMessage);
        return;
    }
    bool dataProcessed = false;
    QString key = filterInfo.value("key");
    int offset = filterInfo.value("offset").toInt();
    QStringList value = results.value(key).split(';', QString::SkipEmptyParts);
    //qDebug()<<"// RESULT; "<<key<<" = "<<value;
    if (filterInfo.contains("resultmessage")) {
        QString mess = filterInfo.value("resultmessage");
        mess.replace("%count", QString::number(value.count()));
        emit displayMessage(mess, 0, InformationMessage);
    }
    else emit displayMessage(i18n("Processing data analysis"), 0, InformationMessage);
    if (filterInfo.contains("cutscenes")) {
        // Check if we want to cut scenes from returned data
        dataProcessed = true;
        int cutPos = 0;
        QUndoCommand *command = new QUndoCommand();
        command->setText(i18n("Auto Split Clip"));
        foreach (const QString &pos, value) {
            if (!pos.contains("=")) continue;
            int newPos = pos.section('=', 0, 0).toInt();
            // Don't use scenes shorter than 1 second
            if (newPos - cutPos < 24) continue;
            (void) new AddClipCutCommand(this, id, cutPos + offset, newPos + offset, QString(), true, false, command);
            cutPos = newPos;
        }
        if (command->childCount() == 0)
            delete command;
        else m_commandStack->push(command);
    }
    if (markersType >= 0) {
        // Add markers from returned data
        dataProcessed = true;
        int cutPos = 0;
        QUndoCommand *command = new QUndoCommand();
        command->setText(i18n("Add Markers"));
        QList <CommentedTime> markersList;
        int index = 1;
        foreach (const QString &pos, value) {
            if (!pos.contains("=")) continue;
            int newPos = pos.section('=', 0, 0).toInt();
            // Don't use scenes shorter than 1 second
            if (newPos - cutPos < 24) continue;
            CommentedTime m(GenTime(newPos + offset, m_fps), QString::number(index), markersType);
            markersList << m;
            index++;
            cutPos = newPos;
        }
        emit addMarkers(id, markersList);
    }
    if (!dataProcessed || filterInfo.contains("storedata")) {
        // Store returned data as clip extra data
        clip->referencedClip()->setAnalysisData(filterInfo.contains("displaydataname") ? filterInfo.value("displaydataname") : key, results.value(key), filterInfo.value("offset").toInt());
        emit updateAnalysisData(clip->referencedClip());
    }
}

// Should be removed (hack to fix projectsettings)
BinController *ProjectList::binController()
{
    return pCore->binController();
}

/*
// Work in progress: apply filter based on clip's camcorder
void ProjectList::checkCamcorderFilters(DocClipBase *clip, QMap <QString, QString> meta)
{
    KConfig conf("camcorderfilters.rc", KConfig::CascadeConfig, "appdata");
    QStringList groups = conf.groupList();
    foreach(const QString &grp, groups) {
    if (!meta.contains(grp)) continue;
    KConfigGroup group(&conf, grp);
    QString value = group.readEntry(meta.value(grp));
    if (value.isEmpty()) continue;
    clip->setProperty(value.section(' ', 0, 0), value.section(' ', 1));
    break;
    }
}*/


